using Cinemachine;
using UnityEngine;

public class MainMenuScene : MonoBehaviour
{
    [SerializeField] private float switchCamDelay = 1;
    [SerializeField] private CinemachineVirtualCamera[] sceneCam;

    private int _currentCam;

    private void Start()
    {
        SwitchCam();
    }

    private void SwitchCam()
    {
        SetCam(GetNextCam());
        LeanTween.value(0, 1, switchCamDelay).setOnComplete(SwitchCam);
    }

    private CinemachineVirtualCamera GetNextCam()
    {
        return sceneCam[_currentCam++ % sceneCam.Length];
    }

    private void SetCam(CinemachineVirtualCamera to)
    {
        foreach (var cam in sceneCam)
            cam.Priority = 0;

        to.Priority = 1;
    }
}