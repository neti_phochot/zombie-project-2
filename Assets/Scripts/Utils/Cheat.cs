﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Player.data;
using UnityEngine;
using ZombieProject.game;

namespace Utils
{
    public class Cheat : MonoBehaviour
    {
        [SerializeField] private GameDebug gameDebug;
        [SerializeField] private UserSaveSO userSaveSo;
        private readonly Dictionary<string, Action> _cheats = new Dictionary<string, Action>();

        private void Awake()
        {
            gameDebug.OnRunCommand += OnRunCommandEvent;
            SetupCheats();
        }

        private void SetupCheats()
        {
            _cheats.Add("help", () =>
            {
                gameDebug.AddLog("----[ Commands ]----");
                gameDebug.AddLog("/god");
                gameDebug.AddLog("/money");
                gameDebug.AddLog("/gold");
                gameDebug.AddLog("/unlockalllevel");
                gameDebug.AddLog("/resetuser");
            });

            _cheats.Add("money", () =>
            {
                FindObjectOfType<WaveManager>().Coin = 99999;
                gameDebug.AddLog("money: Unlimited Money: true");
            });

            _cheats.Add("god", () =>
            {
                FindObjectOfType<WaveManager>().HealthPoint = 99999;
                gameDebug.AddLog("god: God Mode: true");
            });

            _cheats.Add("gold", () =>
            {
                userSaveSo.points = 99999;
                gameDebug.AddLog("gold: Unlimited Gold: true");
            });

            _cheats.Add("unlockalllevel", () =>
            {
                userSaveSo.levelUnlocked = int.MaxValue;
                gameDebug.AddLog("All level unlocked!");
            });

            _cheats.Add("resetuser", () =>
            {
                userSaveSo.Clear();
                gameDebug.AddLog("reset user: Clear user saved");
            });
        }

        private void OnRunCommandEvent(string cmd)
        {
            if (_cheats.ContainsKey(cmd))
                _cheats[cmd].Invoke();
        }
    }
}