﻿using System.IO;
using Player.data;
using UnityEngine;

namespace Utils
{
    public static class SaveManager
    {
        private static readonly string GamePath = Application.persistentDataPath;

        public static void Save(UserSaveSO userSaveSo)
        {
            var json = JsonUtility.ToJson(new UserSaveSO.UserData(userSaveSo));
            File.WriteAllText(GetSavePath(), json);
            Debug.Log("Saved user data!");
        }

        public static void LoadSave(UserSaveSO userSaveSo)
        {
            if (!File.Exists(GetSavePath()))
            {
                userSaveSo.Clear();
                return;
            }

            var userSave = JsonUtility.FromJson<UserSaveSO.UserData>(File.ReadAllText(GetSavePath()));
            userSaveSo.username = userSave.username;
            userSaveSo.levelUnlocked = userSave.levelUnlocked;
            userSaveSo.points = userSave.points;
            //userSaveSo.purchasedItem = userSave.purchasedItem;
            //userSaveSo.equippedItem = userSave.equippedItem;
            Debug.Log("Loaded user data!");
        }

        private static string GetSavePath()
        {
            return Path.Combine(GamePath, "Save.json");
        }
    }
}