﻿using UnityEngine;

namespace Utils
{
    public class RotateOverTime : MonoBehaviour
    {
        [SerializeField] private float rotateSpeed;
        [SerializeField] private Vector3 axis;

        private void FixedUpdate()
        {
            transform.Rotate(axis, rotateSpeed);
        }
    }
}