﻿using System;

namespace Utils
{
    public static class UtilsClass
    {
        public static string TimeMillToString(int time)
        {
            var timeSpan = TimeSpan.FromMilliseconds(time);
            return $"{(int) timeSpan.TotalMinutes:D2}:{timeSpan.Seconds:D2}.{timeSpan.Milliseconds:D3}";
        }
    }
}