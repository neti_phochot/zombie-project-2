﻿using System;
using System.Collections.Generic;
using Singleton;
using UnityEngine;

namespace Utils
{
    public class JUtils : DynamicSingleton<JUtils>
    {
        private readonly List<JValueData> _valueList = new List<JValueData>();

        private void Update()
        {
            for (var i = 0; i < _valueList.Count; i++)
            {
                var data = _valueList[i];
                var isCompleted = data.Process();
                if (isCompleted)
                    _valueList.Remove(data);
            }
        }

        public static JValueData Value(float from, float to, float time)
        {
            var valueData = new JValueData(from, to, time);
            Instance._valueList.Add(valueData);
            return valueData;
        }

        public class JValueData
        {
            private readonly float _time;
            private readonly float _to;
            private float _from;

            private Action _onComplete;
            private Action<float> _onUpdate;

            public JValueData(float from, float to, float time)
            {
                _from = from;
                _to = to;
                _time = time;
            }

            public JValueData setOnUpdate(Action<float> action)
            {
                _onUpdate = action;
                return this;
            }

            public JValueData setOnComplete(Action action)
            {
                _onComplete = action;
                return this;
            }

            internal bool Process()
            {
                var isIncrease = _to > _from;
                var increase = (isIncrease ? Time.deltaTime : -Time.deltaTime) / _time;
                _from += increase;
                _onUpdate?.Invoke(_from);
                if (isIncrease)
                {
                    if (_from < _to) return false;
                }
                else
                {
                    if (_from > _to) return false;
                }

                _onComplete();
                return true;
            }
        }
    }
}