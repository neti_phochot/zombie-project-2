﻿using UnityEngine;

namespace Utils
{
    public class BackgroundPlacer : MonoBehaviour
    {
        private const  int Side = 8;
        [Header("CONFIG")]
        [SerializeField] private SpriteRenderer[] backgroundPrefabs;
        [SerializeField] private float scaleWidth;
        [SerializeField] private float scaleHeight;
        
        [Header("READ ONLY")]
        [SerializeField] private SpriteRenderer[] backgrounds;
        [SerializeField] private float distance;

        [ContextMenu("Create Background")]
        private void CreateBackground()
        {
            foreach (var background in backgrounds)
            {
                DestroyImmediate(background);
            }

            backgrounds = new SpriteRenderer[Side];

            var indexPrefab = 0;
            var angle = 0f;
            for (var i = 0; i < Side; i++)
            {
                var background = Instantiate(backgroundPrefabs[indexPrefab++ % backgroundPrefabs.Length], transform);
                background.transform.Rotate(new Vector3(0, angle, 0));
                backgrounds[i] = background;
                angle += 45f;
            }

            SetBackgroundScale();
        }

        private void SetBackgroundScale()
        {
            foreach (var bg in backgrounds)
            {
                bg.transform.localScale = new Vector3(scaleWidth, scaleHeight, 1f);
                var spriteLength = backgrounds[0].bounds.size.x;
                distance = spriteLength / 2f + (Mathf.Sqrt(2) / 2) * spriteLength;
                bg.transform.position = distance * bg.transform.forward;
            }
        }

        private void OnValidate()
        {
            if (backgrounds.Length == 8)
            {
                SetBackgroundScale();
            }
        }
    }
}