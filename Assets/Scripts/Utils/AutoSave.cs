﻿using Player.data;
using UnityEngine;

namespace Utils
{
    public class AutoSave : MonoBehaviour
    {
        [SerializeField] private UserSaveSO userSaveSo;

        private void Awake()
        {
            SaveManager.LoadSave(userSaveSo);
        }

        private void OnDestroy()
        {
            SaveManager.Save(userSaveSo);
        }
    }
}