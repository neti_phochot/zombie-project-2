﻿using UnityEngine.SceneManagement;

namespace Utils.scene
{
    public static class GameSceneManager
    {
        private const float SceneTransitionLength = 0.5f;
        public static GameSceneType CurrentLevel = GameSceneType.SPLASHSCREEN;

        public static void RestartCurrentScene()
        {
            GamePaused.Continue();
            FadeScreen.FadeOut(SceneTransitionLength).setOnComplete(() =>
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            });
        }

        public static void LoadScene(GameSceneType gameSceneType)
        {
            GamePaused.Continue();
            FadeScreen.FadeOut(SceneTransitionLength).setOnComplete(() =>
            {
                var sceneIndex = (int) gameSceneType;
                SceneManager.LoadScene(sceneIndex);
                CurrentLevel = gameSceneType;
            });
        }
    }
}