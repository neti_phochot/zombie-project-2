﻿namespace Utils.scene
{
    public enum GameSceneType
    {
        SPLASHSCREEN = 0,
        MAINMENU = 1,
        MAP = 2,
        LEVEL_1 = 3,
        LEVEL_2 = 4,
        LEVEL_3 = 5,
    }
}