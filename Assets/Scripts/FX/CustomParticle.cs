﻿using UnityEngine;

namespace FX
{
    public class CustomParticle : MonoBehaviour
    {
        [SerializeField] private EnumValue particleType;
        public EnumValue ParticleType => particleType;
    }
}