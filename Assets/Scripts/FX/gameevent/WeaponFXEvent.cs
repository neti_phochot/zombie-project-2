﻿using System;
using UnityEngine;
using ZombieProject.events;
using ZombieProject.weapon;

namespace FX.gameevent
{
    public class WeaponFXEvent : MonoBehaviour
    {
        [Header("EXPLODE")] [SerializeField] private float explodeAmp = 0.5f;
        [SerializeField] private float explodeShakeLength = 0.2f;
        [Header("WEAPON")] [SerializeField] private float weaponAmp = 0.5f;
        [SerializeField] private float weaponShakeLength = 0.2f;
        [SerializeField] private EnumValue gunFireType;
        [SerializeField] private EnumValue impactType;
        [SerializeField] private float gunFireOffset;
        [SerializeField] private float gunCamImpactRange = 10f;

        private Core.entity.Player _player;

        private void Awake()
        {
            _player = FindObjectOfType<Core.entity.Player>();
        }

        private void OnEnable()
        {
            GameEventInstance.Instance.WeaponEvent.ShootEvent += OnShootEvent;
            GameEventInstance.Instance.WeaponEvent.ExplodeProjectileHitEvent += OnExplodeProjectileHitEvent;
            GameEventInstance.Instance.WeaponEvent.ProjectileHitEvent += OnProjectileHitEvent;
        }


        private void OnDisable()
        {
            GameEventInstance.Instance.WeaponEvent.ShootEvent -= OnShootEvent;
            GameEventInstance.Instance.WeaponEvent.ExplodeProjectileHitEvent -= OnExplodeProjectileHitEvent;
            GameEventInstance.Instance.WeaponEvent.ProjectileHitEvent -= OnProjectileHitEvent;
        }

        private void OnShootEvent(IWeapon weapon)
        {
            PlayTowerFireFX(weapon);

            var distanceToPlayer = Vector3.Distance(weapon.GetShooter().GetLocation(), _player.GetLocation());
            if (distanceToPlayer > gunCamImpactRange) return;
            ShakeCam.Instance.Shake(weaponAmp, weaponShakeLength);
        }

        private void PlayTowerFireFX(IWeapon weapon)
        {
            if (weapon.GetShooter() is Core.entity.Player) return;
            var fx = FXInstance.PlayEffect(gunFireType, weapon.GetShootTransform().position);
            var dir = weapon.GetShootTransform().forward;
            fx.transform.rotation = Quaternion.LookRotation(dir);
            fx.transform.position += dir * gunFireOffset;
        }

        private void OnExplodeProjectileHitEvent(ContactPoint contactPoint)
        {
            ShakeCam.Instance.Shake(explodeAmp, explodeShakeLength);
        }

        private void OnProjectileHitEvent(ContactPoint contactPoint)
        {
            var fx = FXInstance.PlayEffect(impactType, contactPoint.point);
            fx.transform.rotation = Quaternion.LookRotation(contactPoint.normal);
        }
    }
}