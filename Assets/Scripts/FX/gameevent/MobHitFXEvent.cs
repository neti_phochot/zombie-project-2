﻿using Core.entity;
using Core.events;
using UnityEngine;

namespace FX.gameevent
{
    public class MobHitFXEvent : MonoBehaviour
    {
        [SerializeField] private EnumValue bloodParticleType;
        [SerializeField] private EnumValue deadParticleType;
        private void OnEnable()
        {
            EventInstance.Instance.EntityEvent.EntityDamageModifiedEvent += OnEntityDamageModifiedEvent;
            EventInstance.Instance.EntityEvent.EntityDamageEvent += OnEntityDamageEvent;
            EventInstance.Instance.EntityEvent.EntityDeathEvent += OnEntityDeathEvent;
        }

        private void OnDisable()
        {
            EventInstance.Instance.EntityEvent.EntityDamageModifiedEvent -= OnEntityDamageModifiedEvent;
            EventInstance.Instance.EntityEvent.EntityDamageEvent -= OnEntityDamageEvent;
            EventInstance.Instance.EntityEvent.EntityDeathEvent -= OnEntityDeathEvent;
        }

        private void OnEntityDamageEvent(LivingEntity livingEntity, float damage)
        {
            FXInstance.PlayEffect(bloodParticleType, livingEntity.GetLocation());
        }

        private void OnEntityDamageModifiedEvent(LivingEntity livingEntity, float damage, bool modified)
        {
            FXInstance.PlayEffect(bloodParticleType, livingEntity.GetLocation());
        }

        private void OnEntityDeathEvent(LivingEntity livingEntity)
        {
            FXInstance.PlayEffect(deadParticleType, livingEntity.GetLocation());
        }
    }
}