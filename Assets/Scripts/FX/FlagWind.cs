﻿using UnityEngine;
using Random = UnityEngine.Random;

namespace FX
{
    public class FlagWind : MonoBehaviour
    {
        private static Vector3 _windRotate = Vector3.zero;

        private void Awake()
        {
            RandomWindDir();
            transform.rotation = Quaternion.Euler(_windRotate);
        }

        private void RandomWindDir()
        {
            if (_windRotate.magnitude > 0) return;
            _windRotate = new Vector3(0,Random.Range(0f, 360f),0);
        }
    }
}