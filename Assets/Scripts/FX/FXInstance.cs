﻿using System;
using System.Collections.Generic;
using Singleton;
using UnityEngine;

namespace FX
{
    public class FXInstance : ResourceSingleton<FXInstance>
    {
        [SerializeField] private int initParticle;
        [SerializeField] private CustomParticleData[] customParticleData;

        public override void Awake()
        {
            base.Awake();
            InitFXPool();
        }

        private Dictionary<EnumValue, FXPool> _fxPools;

        private void InitFXPool()
        {
            _fxPools = new Dictionary<EnumValue, FXPool>();
            foreach (var data in customParticleData)
            {
                if (_fxPools.ContainsKey(data.customParticle.ParticleType)) continue;
                var pool = data.fxPool;
                pool.Init(data.customParticle);
                pool.Prewarm(initParticle);
                _fxPools.Add(data.customParticle.ParticleType, pool);
            }
        }

        [Serializable]
        public struct CustomParticleData
        {
            public CustomParticle customParticle;
            public FXPool fxPool;
        }

        public static CustomParticle PlayEffect(EnumValue particleType, Vector3 location)
        {
            var pool = Instance._fxPools[particleType];
            var particle = pool.Request();
            pool.Return(particle);
            particle.transform.position = location;
            particle.gameObject.SetActive(true);
            return particle;
        }
    }
}