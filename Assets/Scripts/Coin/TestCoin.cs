﻿using System;
using UnityEngine;

namespace Coin
{
    public class TestCoin : MonoBehaviour
    {
        public int spawnCoinAmount;
        public float padding;

        private Camera _camera;

        private void Awake()
        {
            _camera = Camera.main;
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                SpawnCoin();
            }
        }

        private void SpawnCoin()
        {
            if (Physics.Raycast(_camera.ScreenPointToRay(Input.mousePosition), out var hit))
            {
                WorldItemInstance.Instance.SpawnWorldCoins(() => { Debug.Log("Pick UpItem"); }, spawnCoinAmount,
                    padding,
                    hit.point);
            }
        }
    }
}