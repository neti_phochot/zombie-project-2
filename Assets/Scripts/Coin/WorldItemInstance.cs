﻿using System;
using Singleton;
using UnityEngine;

namespace Coin
{
    public class WorldItemInstance : ResourceSingleton<WorldItemInstance>
    {
        [SerializeField] private WorldItemPool worldItemPool;
        [SerializeField] private WorldItem worldItemPrefab;
        [SerializeField] private int initSize;

        public override void Awake()
        {
            base.Awake();
            worldItemPool.Init(worldItemPrefab);
            worldItemPool.Prewarm(initSize);
        }

        public WorldItem[] SpawnWorldCoins(Action pickupAction, int amount, float padding,
            Vector3 spawnOrigin)
        {
            WorldItem[] worldCoins = new WorldItem[amount];
            var rot = 360f / amount;
            padding *= amount;
            for (int i = 0; i < amount; i++)
            {
                WorldItem worldItem = worldItemPool.Request();
                worldItem.Init(pickupAction);
                worldItem.gameObject.SetActive(true);
                worldItem.transform.rotation = Quaternion.Euler(0, rot * i, 0);
                Vector3 targetLoc = spawnOrigin + worldItem.transform.forward * padding;
                worldItem.DropAt(targetLoc);
                worldCoins[i] = worldItem;
            }

            return worldCoins;
        }

        public void ReturnWorldCoin(WorldItem worldItem)
        {
            worldItemPool.Return(worldItem);
        }
    }
}