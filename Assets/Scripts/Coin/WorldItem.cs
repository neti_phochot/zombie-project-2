﻿using System;
using UnityEngine;

namespace Coin
{
    public class WorldItem : MonoBehaviour
    {
        [SerializeField] private float dropSpeed = 1f;
        [SerializeField] private float dropHigh = 0.5f;
        [SerializeField] private float stayTime = 3f;
        [SerializeField] private float blinkTime = 2f;
        [SerializeField] private int blinkStep = 10;
        [SerializeField] private LeanTweenType dropAnime = LeanTweenType.easeOutBounce;
        [SerializeField] private LeanTweenType blinkAnime;
        [SerializeField] private Transform visualTransform;

        private Action _pickUpAction;

        public void Init(Action pickupAction)
        {
            _pickUpAction = pickupAction;
        }

        private void ResetItem()
        {
            visualTransform.gameObject.SetActive(true);
            WorldItemInstance.Instance.ReturnWorldCoin(this);
        }

        public void DropAt(Vector3 loc)
        {
            LeanTween.cancel(gameObject);

            var spawnLoc = loc;
            spawnLoc.y += dropHigh;
            transform.position = spawnLoc;
            LeanTween.moveY(gameObject, loc.y, dropSpeed)
                .setEase(dropAnime);

            LeanTween.value(gameObject, 0, 1, stayTime)
                .setOnComplete(() =>
                {
                    LeanTween.value(gameObject, 0, blinkStep, blinkTime)
                        .setOnUpdate(v =>
                        {
                            bool isBlink = Mathf.RoundToInt(v) % 2 == 0;
                            visualTransform.gameObject.SetActive(isBlink);
                        })
                        .setEase(blinkAnime)
                        .setOnComplete(ResetItem);
                });
        }

        private void OnTriggerEnter(Collider other)
        {
            _pickUpAction?.Invoke();
            ResetItem();
        }
    }
}