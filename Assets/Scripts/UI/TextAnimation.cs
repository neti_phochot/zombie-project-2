﻿using System;
using UnityEngine;

namespace UI
{
    public class TextAnimation : MonoBehaviour
    {
        [SerializeField] private LeanTweenType animeType;
        [SerializeField] private GameObject textGameObject;
        [SerializeField] private Vector3 offsetFrom;
        [SerializeField] private Vector3 offsetTo;
        [SerializeField] private float time;

        private void Awake()
        {
            SetAnimation();
        }

        private void SetAnimation()
        {
            transform.localScale = offsetFrom;
            LeanTween.scale(textGameObject, offsetTo, time)
                .setEase(animeType)
                .setLoopPingPong();
        }
    }
}