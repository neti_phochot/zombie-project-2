﻿using System.Collections.Generic;

namespace Core.inventory.meta
{
    public class ItemMeta : IItemMeta
    {
        private int _amount;
        private string _displayName;
        private List<string> _lore = new List<string>();

        public string GetDisplayName()
        {
            return _displayName;
        }

        public void SetDisplayName(string displayName)
        {
            _displayName = displayName;
        }

        public List<string> GetLore()
        {
            return _lore;
        }

        public void SetLore(List<string> lore)
        {
            _lore = lore;
        }

        public int GetAmount()
        {
            return _amount;
        }

        public void SetAmount(int amount)
        {
            _amount = amount;
        }
    }
}