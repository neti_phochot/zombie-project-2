﻿using TMPro;
using UnityEngine;

namespace Core.inventory.InventoryView
{
    public class Tooltip : MonoBehaviour, IVisible
    {
        [SerializeField] private TextMeshProUGUI tooltipText;
        [SerializeField] private RectTransform backgroundRect;

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        public void Init(ItemStack itemStack)
        {
            var displayName = itemStack.GetItemMeta().GetDisplayName();
            var lore = string.Join("\n", itemStack.GetItemMeta().GetLore());
            tooltipText.text = $"{displayName}\n\n{lore}";
            tooltipText.ForceMeshUpdate();
            backgroundRect.ForceUpdateRectTransforms();
            var textPadding = 40f;
            var backgroundSize = new Vector2(
                tooltipText.preferredWidth + textPadding * 2f,
                tooltipText.preferredHeight + textPadding * 2f);
            backgroundRect.sizeDelta = backgroundSize;
        }
    }
}