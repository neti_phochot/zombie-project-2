﻿using Core.entity;

namespace Core.inventory.InventoryView
{
    public interface IInventoryView
    {
        InventoryAction GetAction();
        ClickType GetClick();
        void SetCursor(ItemStack itemStack);
        ItemStack GetCursor();
        int GetSlot();
        IInventory GetInventory();
        void Close();
        HumanEntity GetPlayer();
    }
}