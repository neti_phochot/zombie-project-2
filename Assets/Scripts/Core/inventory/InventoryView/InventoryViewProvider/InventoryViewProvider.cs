﻿using UnityEngine;

namespace Core.inventory.InventoryView.InventoryViewProvider
{
    [RequireComponent(typeof(InventoryView))]
    public abstract class InventoryViewProvider : MonoBehaviour
    {
        public abstract void Init(InventoryView inventoryView);
    }
}