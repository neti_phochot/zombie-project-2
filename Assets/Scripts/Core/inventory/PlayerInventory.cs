﻿using Core.events;

namespace Core.inventory
{
    public class PlayerInventory : Inventory, IPlayerInventory
    {
        private int _heldItemSlot;

        public PlayerInventory(int size, InventoryType inventoryType) : base(size, inventoryType)
        {
            EventInstance.Instance.InventoryEvent.OnInventoryUpdateEvent(this);
        }

        public ItemStack GetItemInHand()
        {
            return GetItem(_heldItemSlot);
        }

        public void SetItemInHand(ItemStack itemStack)
        {
            SetItem(_heldItemSlot, itemStack);
        }

        public void SetHeldItemSlot(int slot)
        {
            _heldItemSlot = slot;
        }

        public int GetHeldItemSlot()
        {
            return _heldItemSlot;
        }

        public override void AddItem(ItemStack itemStack)
        {
            base.AddItem(itemStack);
            EventInstance.Instance.InventoryEvent.OnInventoryUpdateEvent(this);
        }

        public override void Remove(ItemStack itemStack)
        {
            base.Remove(itemStack);
            EventInstance.Instance.InventoryEvent.OnInventoryUpdateEvent(this);
        }

        public override void SetItem(int index, ItemStack itemStack)
        {
            base.SetItem(index, itemStack);
            EventInstance.Instance.InventoryEvent.OnInventoryUpdateEvent(this);
        }

        public override void SetStorageContents(ItemStack[] items)
        {
            base.SetStorageContents(items);
            EventInstance.Instance.InventoryEvent.OnInventoryUpdateEvent(this);
        }
    }
}