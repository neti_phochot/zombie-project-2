﻿using Core.entity;
using UnityEngine;

namespace Core.projectile.rayprojectile
{
    public class ProjectileRay : ProjectileBase
    {
        [SerializeField] private float maxDistance;

        public override void Init(Entity shooter, Transform shootTransform, float damage, Vector3 velocity,
            ProjectileType projectileType)
        {
            base.Init(shooter, shootTransform, damage, velocity, projectileType);
            OnHit();
        }

        private void OnHit()
        {
            ProjectileInstance.Return(this);
            var rayTransform = transform;
            Physics.Raycast(rayTransform.position, rayTransform.forward, out var hit, maxDistance);
            if (!hit.collider)
            {
                return;
            }

            if (!hit.collider.transform.TryGetComponent<IDamageable>(out var damageable)) return;
            if (Shooter) damageable.Damage(GetDamage(), Shooter);
            else damageable.Damage(GetDamage());
        }
    }
}