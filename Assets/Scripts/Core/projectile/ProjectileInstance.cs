﻿using System;
using System.Collections.Generic;
using Core.projectile.pool;
using Core.projectile.rayprojectile;
using Singleton;
using UnityEngine;

namespace Core.projectile
{
    public class ProjectileInstance : ResourceSingleton<ProjectileInstance>
    {
        [SerializeField] private ProjectileData[] projectileData;
        private Dictionary<ProjectileType, ProjectilePool> _projectiles;

        public override void Awake()
        {
            base.Awake();
            InitProjectile();
        }

        private void InitProjectile()
        {
            _projectiles = new Dictionary<ProjectileType, ProjectilePool>();
            foreach (var data in projectileData)
            {
                if (_projectiles.ContainsKey(data.projectileType)) continue;
                var projectilePool = new GameObject($"{data.projectileType} [Pooling Projectile]")
                    .AddComponent<ProjectilePool>();
                projectilePool.Init(data.projectilePrefab);
                projectilePool.Prewarm(1);
                projectilePool.transform.SetParent(transform);
                _projectiles.Add(data.projectileType, projectilePool);
            }
        }

        public static ProjectileBase Create(ProjectileType projectileType)
        {
            if (!Instance._projectiles.ContainsKey(projectileType)) return null;
            var projectile = Instance._projectiles[projectileType].Request();
            return projectile;
        }

        public static void Return(ProjectileBase projectile)
        {
            Instance._projectiles[projectile.GetProjectileType()].Return(projectile);
        }

        [Serializable]
        public struct ProjectileData
        {
            public ProjectileType projectileType;
            public ProjectileBase projectilePrefab;
        }
    }
}