﻿using Core.entity;
using Core.projectile.rayprojectile;
using UnityEngine;
using ZombieProject.events;

namespace Core.projectile
{
    [RequireComponent(typeof(Rigidbody))]
    public class Projectile : ProjectileBase
    {
        private Rigidbody _rigidbody;

        protected override void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
        }

        protected override void FixedUpdate()
        {
            var velocity = _rigidbody.velocity;
            if (velocity.magnitude > 0f) transform.rotation = Quaternion.LookRotation(velocity.normalized);
        }

        private void OnCollisionEnter(Collision other)
        {
            ProjectileInstance.Return(this);
            GameEventInstance.Instance.WeaponEvent.OnProjectileHitEvent(other.contacts[0]);

            if (!other.transform.TryGetComponent<IDamageable>(out var damageable)) return;
            var (damage, isDamageModified) = GetCalculatedDamage(GetDamage(), damageable.GetArmorType());
            if (Shooter) damageable.Damage(damage, Shooter, isDamageModified);
            else damageable.Damage(damage);
        }

        public override void Init(Entity shooter, Transform shootTransform, float damage, Vector3 velocity,
            ProjectileType projectileType)
        {
            base.Init(shooter, shootTransform, damage, velocity, projectileType);

            _rigidbody.angularVelocity = Vector3.zero;
            _rigidbody.velocity = Vector3.zero;
            _rigidbody.AddForce(velocity, ForceMode.VelocityChange);
        }
    }
}