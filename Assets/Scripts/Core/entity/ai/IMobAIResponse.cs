﻿namespace Core.entity.ai
{
    public interface IMobAIResponse
    {
        void OnMobAIUpdate(Mob mob);
    }
}