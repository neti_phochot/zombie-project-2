﻿using Core.entity.ai;

namespace Core.entity
{
    public class Mob : LivingEntity
    {
        private IMobAIResponse _mobAIResponse;
        private IMobTargetResponse _mobTargetResponse;

        private LivingEntity _target;

        protected override void Awake()
        {
            base.Awake();
            _mobTargetResponse = GetComponent<IMobTargetResponse>();
            _mobAIResponse = GetComponent<IMobAIResponse>();
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            _mobAIResponse.OnMobAIUpdate(this);
        }

        public LivingEntity GetTarget()
        {
            return _target;
        }

        public void SetTarget(LivingEntity target)
        {
            _target = target;
            _mobTargetResponse.OnMobTarget(this, target);
        }

        public bool HasTarget()
        {
            return _target && !_target.IsDead();
        }

        public void ClearTarget()
        {
            _target = null;
            _mobTargetResponse.OnMobTargetClear(this);
        }
    }
}