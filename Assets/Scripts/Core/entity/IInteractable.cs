﻿using Core.events;

namespace Core.entity
{
    public interface IInteractable
    {
        void Interact(ActionType actionType, Entity source);
    }
}