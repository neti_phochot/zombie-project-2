﻿using System.Collections.Generic;
using Singleton;
using UnityEngine;

namespace Core.entity
{
    public class EntityInstance : ResourceSingleton<EntityInstance>
    {
        [SerializeField] private Entity[] entities;
        private Dictionary<EntityType, Entity> _entitiesPrefab;

        public override void Awake()
        {
            base.Awake();
            InitEntity();
        }

        private void InitEntity()
        {
            _entitiesPrefab = new Dictionary<EntityType, Entity>();
            foreach (var entityPrefab in entities) _entitiesPrefab.Add(entityPrefab.GetEntityType(), entityPrefab);
        }

        public Entity SpawnEntity(EntityType entityType, Vector3 location)
        {
            var entity = Instantiate(_entitiesPrefab[entityType]);
            entity.SetLocation(location);
            return entity;
        }
    }
}