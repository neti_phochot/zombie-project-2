﻿using UnityEngine;

namespace Core.entity.mob
{
    public class Dummy : LivingEntity
    {
        [SerializeField] private float jumpHigh;
        [SerializeField] private float speed;
        [SerializeField] private LeanTweenType leanTweenType;
        private void Start()
        {
            LeanTween.moveY(gameObject, transform.position.y + jumpHigh, speed)
                .setEase(leanTweenType)
                .setLoopPingPong();
        }
    }
}