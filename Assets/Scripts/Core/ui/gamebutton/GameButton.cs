﻿using System;
using Core.events;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Core.ui.gamebutton
{
    [RequireComponent(typeof(Image))]
    public class GameButton : MonoBehaviour, IGameButton
        , IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler,
        IPointerClickHandler
    {
        [Header("BUTTON STATE")] [SerializeField]
        private Sprite hover;

        [SerializeField] private Sprite click;

        private Action _clickAction;
        private Action _hoverAction;
        private Action _exitAction;
        private Image _buttonImage;
        private Sprite _normal;

        private Image ButtonImages
        {
            get
            {
                if (!_buttonImage)
                    _buttonImage = GetComponent<Image>();
                return _buttonImage;
            }
        }

        protected virtual void Awake()
        {
            _normal = ButtonImages.sprite;
        }

        protected virtual void Start()
        {
            if (!_normal) return;
            ButtonImages.sprite = _normal;
        }

        public void SetImageColor(Color color)
        {
            ButtonImages.color = color;
        }

        public void SetNormalImage(Sprite sprite)
        {
            _normal = sprite;
            ButtonImages.sprite = sprite;
        }

        public void SetHoverImage(Sprite sprite)
        {
            _normal = sprite;
        }

        public void SetClickImage(Sprite sprite)
        {
            _normal = sprite;
        }

        public void SetClickAction(Action action)
        {
            _clickAction = action;
        }

        public void SetHoverAction(Action action)
        {
            _hoverAction = action;
        }
        public void SetExitAction(Action action)
        {
            _exitAction = action;
        }
        
        public void OnPointerEnter(PointerEventData eventData)
        {
            _hoverAction?.Invoke();
            if (!hover) return;
            ButtonImages.sprite = hover;
            EventInstance.Instance.UIEvent.OnButtonEnterEvent();
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            _exitAction?.Invoke();
            if (!_normal) return;
            ButtonImages.sprite = _normal;
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            if (!click) return;
            ButtonImages.sprite = click;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            _clickAction?.Invoke();
            EventInstance.Instance.UIEvent.OnButtonClickEvent();
        }
    }
}