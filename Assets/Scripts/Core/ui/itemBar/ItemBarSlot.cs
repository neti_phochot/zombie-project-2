﻿using Core.inventory;
using ScriptableObjects.Inventory;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Core.ui.itemBar
{
    public class ItemBarSlot : MonoBehaviour
    {
        [SerializeField] private ItemIconPack itemIconPack;
        [SerializeField] private Image itemIcon;
        [SerializeField] private Transform indicator;
        [SerializeField] private TextMeshProUGUI itemAmountText;

        private void Awake()
        {
            SetIndicator(false);
        }

        public void Init(ItemStack itemStack)
        {
            itemIcon.sprite = itemIconPack.GetItemIcon(itemStack?.GetType() ?? itemIconPack.GetDefault());
            var itemAmount = itemStack == null || itemStack.GetItemMeta().GetAmount() < 1
                ? string.Empty
                : $"{itemStack.GetItemMeta().GetAmount()}";
            itemAmountText.text = $"{itemAmount}";
        }

        public void SetIndicator(bool show)
        {
            indicator.gameObject.SetActive(show);
        }
    }
}