﻿using Core.ui.gamebutton;
using Player.data;
using ScriptableObjects.Inventory;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ZombieProject.weapon;

namespace Core.ui.weaponshop
{
    public class WeaponShopMenu : MonoBehaviour, IWeaponShopMenu
    {
        [SerializeField] private UserSaveSO userSaveSo;
        [SerializeField] private Color greyOutColor;
        [Space] [SerializeField] private ItemShopSlot weaponShopItemSlotTemplate;
        [SerializeField] private Transform container;
        [SerializeField] private GameButton backButton;

        [Header("SHOP ITEM")] [SerializeField] private WeaponSo[] weaponSo;

        [Header("SHOP SETTINGS")] [SerializeField]
        private int maxEquipItem;

        [SerializeField] private ItemIconPack itemIconPack;
        [SerializeField] private Image itemShowcaseImage;
        [Header("SHOP TEXT")] [SerializeField] private TextMeshProUGUI myPointText;
        [SerializeField] private TextMeshProUGUI equipCountText;
        [SerializeField] private TextMeshProUGUI itemNameText;
        [SerializeField] private TextMeshProUGUI itemInfoText;
        [SerializeField] private TextMeshProUGUI itemPriceText;

        [Header("SHOP BUTTON")] [SerializeField]
        private GameButton equipButton;

        [SerializeField] private GameButton unEquipButton;
        [SerializeField] private GameButton buyButton;

        private void Awake()
        {
            backButton.SetClickAction(Hide);
            itemNameText.text = string.Empty;
            itemInfoText.text = string.Empty;
            itemPriceText.text = string.Empty;
        }

        private void Start()
        {
            InitItemShop();
            RefreshItemShop();
        }

        private ItemShopSlot[] _itemShopSlots;

        private void InitItemShop()
        {
            _itemShopSlots = new ItemShopSlot[weaponSo.Length];
            weaponShopItemSlotTemplate.gameObject.SetActive(true);
            for (var i = 0; i < weaponSo.Length; i++)
            {
                var itemShopSlot = Instantiate(weaponShopItemSlotTemplate, container);
                itemShopSlot.Weapon = weaponSo[i];
                _itemShopSlots[i] = itemShopSlot;
            }

            weaponShopItemSlotTemplate.gameObject.SetActive(false);
        }

        private void RefreshItemShop()
        {
            myPointText.text = $"{userSaveSo.points} Gold";
            equipCountText.text = $"Equipment: {userSaveSo.equippedItem.Count}/{maxEquipItem}";

            buyButton.gameObject.SetActive(false);
            unEquipButton.gameObject.SetActive(false);
            equipButton.gameObject.SetActive(false);

            foreach (var itemShopSlot in _itemShopSlots)
            {
                var weapon = itemShopSlot.Weapon;
                var canPurchase = userSaveSo.points >= weapon.price;
                var isPurchased = userSaveSo.purchasedItem.Contains(weapon);
                var isEquipped = userSaveSo.equippedItem.Contains(weapon);
                itemShopSlot.SetPurchased(isPurchased);
                itemShopSlot.SetEquipped(isEquipped);
                itemShopSlot.SetIndicator(false);
                itemShopSlot.SlotButton.SetImageColor(isPurchased || canPurchase ? Color.white : greyOutColor);
                itemShopSlot.SlotButton.SetClickImage(itemIconPack.GetItemIcon(weapon.enumValue));
                itemShopSlot.SlotButton.SetClickAction(() =>
                {
                    RefreshItemShop();
                    OnItemShopClicked(weapon);
                    itemShopSlot.SetIndicator(true);

                    buyButton.gameObject.SetActive(canPurchase && !isPurchased);
                    buyButton.SetClickAction(() =>
                    {
                        OnItemShopPurchased(weapon);
                        RefreshItemShop();

                        buyButton.gameObject.SetActive(false);
                    });

                    var isMaxEquipReacted = userSaveSo.equippedItem.Count < maxEquipItem;
                    equipButton.gameObject.SetActive(!isEquipped && isPurchased && isMaxEquipReacted);
                    equipButton.SetClickAction(() =>
                    {
                        OnEquipped(weapon);
                        RefreshItemShop();

                        unEquipButton.gameObject.SetActive(true);
                    });

                    unEquipButton.gameObject.SetActive(isEquipped && isPurchased);
                    unEquipButton.SetClickAction(() =>
                    {
                        OnUnEquipped(weapon);
                        RefreshItemShop();

                        equipButton.gameObject.SetActive(true);
                    });
                });
            }
        }

        private void OnEquipped(WeaponSo weapon)
        {
            userSaveSo.equippedItem.Add(weapon);
        }

        private void OnUnEquipped(WeaponSo weapon)
        {
            userSaveSo.equippedItem.Remove(weapon);
        }

        private void OnItemShopPurchased(WeaponSo weapon)
        {
            userSaveSo.points -= weapon.price;
            userSaveSo.purchasedItem.Add(weapon);
        }

        private void OnItemShopClicked(WeaponSo weapon)
        {
            itemShowcaseImage.sprite = itemIconPack.GetItemIcon(weapon.enumValue);
            itemNameText.text = weapon.displayName;
            var weaponState = new[]
            {
                $"Ammo: {weapon.maxAmmo}",
                $"Reload Time: {weapon.reloadTime}",
                $"Fire Rate: {weapon.fireRate}s",
            };
            itemInfoText.text = string.Join("\n", weaponState);
            itemPriceText.text = $"{weapon.price}";
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}