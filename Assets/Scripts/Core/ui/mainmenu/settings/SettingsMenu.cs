﻿using Core.ui.gamebutton;
using Player.data;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace Core.ui.mainmenu.settings
{
    public class SettingsMenu : MonoBehaviour, IVisible
    {
        [SerializeField] public UserOptionSO userOption;
        [SerializeField] private GameButton backButton;

        [Header("MUSIC BUTTON")] [SerializeField]
        private GameButton musicButton;

        [SerializeField] private Image musicImage;
        [SerializeField] private Sprite musicOnSprite;
        [SerializeField] private Sprite musicOffSprite;

        [Header("SOUND BUTTON")] [SerializeField]
        private GameButton soundButton;

        [SerializeField] private Image soundImage;
        [SerializeField] private Sprite soundOnSprite;
        [SerializeField] private Sprite soundOffSprite;

        [Header("SENSITIVITY BUTTON")] [SerializeField]
        private Slider sensitivitySlider;

        [SerializeField] private TextMeshProUGUI sensitivityText;

        protected virtual void Awake()
        {
        }

        protected virtual void Start()
        {
            backButton.SetClickAction(Hide);
            musicButton.SetClickAction(ToggleMusic);
            soundButton.SetClickAction(ToggleSound);
            sensitivitySlider.onValueChanged.AddListener(OnSensitivityChanged);

            UpdateMusicSettings();
            UpdateSoundSettings();
            UpdateSensitivitySettings();
        }

        public virtual void Show()
        {
            gameObject.SetActive(true);
        }

        public virtual void Hide()
        {
            gameObject.SetActive(false);
        }

        private void ToggleMusic()
        {
            userOption.IsMusicOn = !userOption.IsMusicOn;
            UpdateMusicSettings();
        }

        private void ToggleSound()
        {
            userOption.IsSoundOn = !userOption.IsSoundOn;
            UpdateSoundSettings();
        }

        protected virtual void OnSensitivityChanged(float value)
        {
            userOption.Sensitivity = value;
            UpdateSensitivitySettings();
        }

        private void UpdateMusicSettings()
        {
            musicImage.sprite = userOption.IsMusicOn ? musicOnSprite : musicOffSprite;
        }

        private void UpdateSoundSettings()
        {
            soundImage.sprite = userOption.IsSoundOn ? soundOnSprite : soundOffSprite;
        }

        private void UpdateSensitivitySettings()
        {
            sensitivitySlider.value = userOption.Sensitivity;

            var sensitivityDisplay = userOption.Sensitivity * 100f;
            sensitivityText.text = $"Sensitivity: {sensitivityDisplay:F0}%";
        }
    }
}