﻿using Core.ui.gamebutton;
using UnityEngine;

namespace Core.ui.mainmenu.about
{
    public class AboutMenu : MonoBehaviour, IVisible
    {
        [SerializeField] private GameButton backButton;
        [SerializeField] private GameButton aboutButton;
        [SerializeField] private GameButton testersButton;
        [SerializeField] private GameButton gameAssetsButton;
        [SerializeField] private GameObject[] infoTextBox;

        private void Awake()
        {
            backButton.SetClickAction(Hide);
            aboutButton.SetClickAction(() => { ShowTextBox(0); });
            testersButton.SetClickAction(() => { ShowTextBox(1); });
            gameAssetsButton.SetClickAction(() => { ShowTextBox(2); });
        }

        private void Start()
        {
            ShowTextBox(0);
        }

        private void ShowTextBox(int index)
        {
            HideInfo();
            infoTextBox[index].SetActive(true);
        }

        private void HideInfo()
        {
            foreach (var obj in infoTextBox)
            {
                obj.SetActive(false);
            }
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}