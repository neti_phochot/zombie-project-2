﻿using Core.ui.gamebutton;
using Core.ui.mainmenu.about;
using Core.ui.mainmenu.leaderboard;
using Core.ui.mainmenu.settings;
using UnityEngine;
using Utils.scene;

namespace Core.ui.mainmenu.main
{
    public class MainMenu : MonoBehaviour, IVisible
    {
        [SerializeField] private GameButton startButton;
        [SerializeField] private GameButton aboutButton;
        [SerializeField] private GameButton settingsButton;
        [SerializeField] private GameButton quitButton;
        [SerializeField] private GameButton leaderboardButton;
        [SerializeField] private SettingsMenu settingsMenu;
        [SerializeField] private AboutMenu aboutMenu;
        [SerializeField] private LeaderBoardMenu leaderBoardMenu;

        private void Awake()
        {
            Debug.Assert(settingsMenu != null, "ISettingsMenu!=null");

            startButton.SetClickAction(() => { GameSceneManager.LoadScene(GameSceneType.MAP); });
            settingsButton.SetClickAction(settingsMenu.Show);
            aboutButton.SetClickAction(aboutMenu.Show);
            leaderboardButton.SetClickAction(leaderBoardMenu.Show);
            quitButton.SetClickAction(Application.Quit);
        }

        private void Start()
        {
            settingsMenu.Hide();
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}