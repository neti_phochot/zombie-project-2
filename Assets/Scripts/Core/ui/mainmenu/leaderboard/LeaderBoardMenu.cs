﻿using System;
using Core.ui.gamebutton;
using TMPro;
using UnityEngine;
using Utils.scene;

namespace Core.ui.mainmenu.leaderboard
{
    public class LeaderBoardMenu : MonoBehaviour, IVisible
    {
        [SerializeField] private GameButton backButton;
        [SerializeField] private TextMeshProUGUI leaderboardLabel;
        [SerializeField] private bool enableLeaderboard;
        [SerializeField] private LeaderboardData[] leaderboardData;
        [SerializeField] private SlotLeaderboard slotLeaderboardTemplate;

        [SerializeField] private GameButton nextButton;
        [SerializeField] private GameButton previousButton;

        private int _currentIndex = -1;

        [Serializable]
        public class LeaderboardData
        {
            public string levelName;
            public bool isLoaded;
            public int leaderboardSize;
            public GameSceneType levelType;
            public Transform levelTransform;
            public Transform slotContainer;
        }

        private void Awake()
        {
            backButton.SetClickAction(Hide);
            nextButton.SetClickAction(Next);
            previousButton.SetClickAction(Previous);
            Next();

            slotLeaderboardTemplate.gameObject.SetActive(false);
        }

        private void Next()
        {
            HideAllLeaderboard();
            _currentIndex = ++_currentIndex % leaderboardData.Length;
            UpdateData(leaderboardData[_currentIndex]);
        }

        private void Previous()
        {
            HideAllLeaderboard();
            if (--_currentIndex < 0)
                _currentIndex = leaderboardData.Length - 1;
            UpdateData(leaderboardData[_currentIndex]);
        }

        private void UpdateData(LeaderboardData data)
        {
            leaderboardLabel.text = data.levelName;
            //SHOW LEADERBOARD
            data.levelTransform.gameObject.SetActive(true);

            if (data.isLoaded) return;
            //INIT LEADERBOARD
            data.isLoaded = true;
            LoadLeaderBoard(data.levelType, data.leaderboardSize, data.slotContainer);
        }

        private void HideAllLeaderboard()
        {
            foreach (var data in leaderboardData)
            {
                data.levelTransform.gameObject.SetActive(false);
            }
        }

        private void ClearSlot(Transform slotTransform)
        {
            foreach (Transform slot in slotTransform)
                Destroy(slot.gameObject);
        }

        private void LoadLeaderBoard(GameSceneType levelType, int count, Transform container)
        {
            if (!enableLeaderboard) return;
            ClearSlot(container);
            LeaderboardInstance.Instance.GetLeaderboard(levelType, count, response =>
            {
                slotLeaderboardTemplate.gameObject.SetActive(true);
                for (int i = 0; i < count; i++)
                {
                    if (!response.success) break;

                    var slot = Instantiate(slotLeaderboardTemplate, container);
                    if (i >= response.items.Length)
                    {
                        slot.Init(i + 1, "-", "???");
                    }
                    else
                    {
                        var item = response.items[i];
                        slot.Init(i + 1, item.member_id, $"{item.score:N0}");
                    }
                }

                slotLeaderboardTemplate.gameObject.SetActive(false);
            });
        }


        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}