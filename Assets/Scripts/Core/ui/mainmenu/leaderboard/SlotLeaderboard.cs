﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Core.ui.mainmenu.leaderboard
{
    public class SlotLeaderboard : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI rankText;
        [SerializeField] private TextMeshProUGUI usernameText;
        [SerializeField] private TextMeshProUGUI timeText;
        [Header("Place Background")]
        [SerializeField] private Image[] buttonImages;
        [SerializeField] private Sprite firstImage;
        [SerializeField] private Sprite secondImage;
        [SerializeField] private Sprite thirdImage;
        [Header("Place Icon")]
        [SerializeField] private Image placeIcon;
        [SerializeField] private Sprite firstPlaceIcon;
        [SerializeField] private Sprite secondPlaceIcon;
        [SerializeField] private Sprite thirdPlaceIcon;

        public void Init(int rank, string userName, string state)
        {
            var placeImage = buttonImages[0].sprite;
            var icon = placeIcon.sprite;
            
            var placeTag = "th";

            switch (rank)
            {
                case 1:
                    placeTag = "st";
                    placeImage = firstImage;
                    icon = firstPlaceIcon;
                    break;
                case 2:
                    placeTag = "nd";
                    placeImage = secondImage;
                    icon = secondPlaceIcon;
                    break;
                case 3:
                    placeTag = "rd";
                    placeImage = thirdImage;
                    icon = thirdPlaceIcon;
                    break;
            }

            foreach (var image in buttonImages)
            {
                image.sprite = placeImage;
            }

            placeIcon.sprite = icon;
            rankText.text = $"{rank}{placeTag}";
            usernameText.text = userName;
            timeText.text = state;
        }
    }
}