﻿using System;
using System.Linq;
using LootLocker.Requests;
using Player.data;
using Singleton;
using UnityEngine;
using Utils.scene;
using Debug = UnityEngine.Debug;

namespace Core.ui.mainmenu.leaderboard
{
    public class LeaderboardInstance : ResourceSingleton<LeaderboardInstance>
    {
        private static bool _isInit;

        [SerializeField] private UserSaveSO userSaveSo;
        [SerializeField] private LeaderboardData[] leaderboardData;

        [Serializable]
        public struct LeaderboardData
        {
            public GameSceneType levelType;
            public int leaderboardId;
        }

        public override void Awake()
        {
            base.Awake();
            InitGuest();
        }

        private void InitGuest()
        {
            if (_isInit) return;
            LootLockerSDKManager.StartGuestSession((response) =>
            {
                if (!response.success)
                {
                    Debug.Log("error starting LootLocker session");
                    return;
                }
                
                _isInit = true;
                Debug.Log("successfully started LootLocker session");
            });
        }

        public void AddScore(GameSceneType levelType, int score, Action<LootLockerSubmitScoreResponse> submitResponse)
        {
            if (string.IsNullOrEmpty(userSaveSo.username)) return;
            var leaderboardId = GetLeaderBoardId(levelType);
            if (leaderboardId == 0)
            {
                Debug.Log("ERROR AddScore");
                return;
            }

            LootLockerSDKManager.SubmitScore(userSaveSo.username, score, leaderboardId, submitResponse);
        }

        public void GetLeaderboard(GameSceneType levelType, int count,
            Action<LootLockerGetScoreListResponse> scoreResponse)
        {
            var leaderboardId = GetLeaderBoardId(levelType);
            if (leaderboardId == 0)
            {
                Debug.Log("ERROR GetLeaderboard");
                return;
            }

            LootLockerSDKManager.GetScoreList(leaderboardId, count, scoreResponse);
        }

        private int GetLeaderBoardId(GameSceneType levelType)
        {
            return (from data in leaderboardData where data.levelType == levelType select data.leaderboardId)
                .FirstOrDefault();
        }
    }
}