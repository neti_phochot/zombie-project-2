﻿using Core.ui.gamebutton;
using Player.data;
using TMPro;
using UnityEngine;

namespace Core.ui.mainmenu.leaderboard
{
    public class NameChanger : MonoBehaviour
    {
        [SerializeField] private int maxNameLenght = 15;
        [SerializeField] private UserSaveSO userSaveSo;
        [SerializeField] private GameButton editButton;

        [SerializeField] private Transform editNameBox;
        [SerializeField] private GameButton renameButton;
        [SerializeField] private GameButton cancelButton;
        [SerializeField] private TMP_InputField nameInput;
        [SerializeField] private TextMeshProUGUI usernameText;

        private void Awake()
        {
            editButton.SetClickAction(() => { ShowEditBox(!editNameBox.gameObject.activeSelf); });
            nameInput.onValueChanged.AddListener(OnUsernameChanged);
            nameInput.onSubmit.AddListener(OnSubmitUsername);
            renameButton.SetClickAction(() => { OnSubmitUsername(nameInput.text); });
            cancelButton.SetClickAction(CheckUsername);
            usernameText.text = string.Empty;
        }

        private void Start()
        {
            CheckUsername();
        }

        private void OnSubmitUsername(string username)
        {
            var checkedName = username.Replace(" ", "_");
            userSaveSo.username = checkedName;
            CheckUsername();
        }

        private void OnUsernameChanged(string username)
        {
            if (username.Length > maxNameLenght)
            {
                nameInput.text = username.Substring(0, username.Length - 1);
            }
        }

        private void ShowEditBox(bool visible)
        {
            editNameBox.gameObject.SetActive(visible);

            if (visible)
            {
                nameInput.text = userSaveSo.username;
            }
        }

        private void CheckUsername()
        {
            if (string.IsNullOrEmpty(userSaveSo.username))
            {
                ShowEditBox(true);
                return;
            }

            usernameText.text = $"Welcome, {userSaveSo.username}!";
            ShowEditBox(false);
        }
    }
}