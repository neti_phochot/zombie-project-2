﻿using Core.ui.gamebutton;
using UnityEngine;
using Utils.scene;

namespace Core.ui.gamemap
{
    public class MapButton : GameButton
    {
        [Header("MAP INDEX")] [SerializeField] private GameSceneType levelType;
       [SerializeField] private Transform marker;

       protected override void Awake()
       {
           base.Awake();
           HideMaker();
       }

       public GameSceneType GetLevelType()
        {
            return levelType;
        }

        public void ShowMaker()
        {
            marker.gameObject.SetActive(true);
        }

        public void HideMaker()
        {
            marker.gameObject.SetActive(false);
        }
    }
}