﻿using Core.ui.gamebutton;
using Core.ui.weaponshop;
using Player.data;
using UnityEngine;
using Utils;
using Utils.scene;

namespace Core.ui.gamemap
{
    public class GameMapManager : MonoBehaviour
    {
        [SerializeField] private GameButton homeButton;
        [SerializeField] private GameButton deleteButton;
        [SerializeField] private UserSaveSO userSaveSo;
        [SerializeField] private MapButton[] mapButtons;

        [Header("WEAPON SHOP")] [SerializeField]
        private GameButton weaponShopButton;

        [SerializeField] private WeaponShopMenu weaponShopMenu;

        [Header("MAP BUTTON ICON")] [SerializeField]
        private Sprite unlockedMapSprite;

        [SerializeField] private Sprite lockedMapSprite;

        private void Start()
        {
            homeButton.SetClickAction(() => { GameSceneManager.LoadScene(GameSceneType.MAINMENU); });
            deleteButton.SetClickAction(() =>
            {
                userSaveSo.Clear();
                SaveManager.Save(userSaveSo);
                GameSceneManager.LoadScene(GameSceneType.MAP);
            });
            weaponShopButton.SetClickAction(weaponShopMenu.Show);

            InitMapButton();
        }

        private void InitMapButton()
        {
            for (var i = 0; i < mapButtons.Length; i++)
            {
                var mapButton = mapButtons[i];
                var isUnlocked = userSaveSo.levelUnlocked >= (int) mapButton.GetLevelType() || i == 0;
                mapButton.SetNormalImage(isUnlocked ? unlockedMapSprite : lockedMapSprite);

                if (isUnlocked)
                {
                    mapButton.SetClickAction(() => { GameSceneManager.LoadScene(mapButton.GetLevelType()); });
                }

                mapButton.SetHoverAction(() => { mapButton.ShowMaker(); });

                mapButton.SetExitAction(() => { mapButton.HideMaker(); });
            }
        }
    }
}