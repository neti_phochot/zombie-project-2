﻿using UnityEngine;
using Utils.scene;

namespace Core.ui.splashscreen
{
    public class SplashScreen : MonoBehaviour
    {
        [SerializeField] private GameSceneType loadScene; 
        [SerializeField] private float loadDelay = 2f;
        private void Awake()
        {
            Invoke(nameof(LoadMenu), loadDelay);
        }

        private void LoadMenu()
        {
            GameSceneManager.LoadScene(loadScene);
        }
    }
}