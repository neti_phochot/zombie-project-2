﻿using Singleton;
using UnityEngine;

namespace Core.events
{
    public class EventInstance : ResourceSingleton<EventInstance>
    {
        [SerializeField] private EntityEventSO entityEventSo;
        [SerializeField] private InputEventSO inputEventSo;
        [SerializeField] private InventoryEventSO inventoryEvent;
        [SerializeField] private PlayerEventSO playerEventSo;
        [SerializeField] private UIEventSO uiEventSo;

        public InventoryEventSO InventoryEvent => inventoryEvent;
        public UIEventSO UIEvent => uiEventSo;
        public InputEventSO InputEvent => inputEventSo;
        public PlayerEventSO PlayerEvent => playerEventSo;
        public EntityEventSO EntityEvent => entityEventSo;
    }
}