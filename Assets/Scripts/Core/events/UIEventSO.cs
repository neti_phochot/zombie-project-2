﻿using System;
using UnityEngine;

namespace Core.events
{
    [CreateAssetMenu(menuName = "Event/UI Event")]
    public class UIEventSO : ScriptableObject
    {
        public event Action ButtonClickEvent;
        public event Action ButtonEnterEvent;
        public event Action<bool> ToggleUIEvent;

        public void OnButtonClickEvent()
        {
            ButtonClickEvent?.Invoke();
        }

        public void OnButtonEnterEvent()
        {
            ButtonEnterEvent?.Invoke();
        }

        public void OnToggleUIEvent(bool visible)
        {
            ToggleUIEvent?.Invoke(visible);
        }

        public event Action<bool> ToggleMenuEvent;

        public void OnToggleMenuEvent(bool paused)
        {
            ToggleMenuEvent?.Invoke(paused);
        }
    }
}