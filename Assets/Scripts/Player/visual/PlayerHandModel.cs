﻿using Core.events;
using Core.inventory;
using UnityEngine;
using ZombieProject.weapon.ui;

namespace Player.visual
{
    [RequireComponent(typeof(PlayerHand))]
    public class PlayerHandModel : MonoBehaviour
    {
        [SerializeField] private HandItemPack handItemPack;
        private PlayerHand _playerHand;

        private void Awake()
        {
            _playerHand = GetComponent<PlayerHand>();
        }

        private void OnEnable()
        {
            EventInstance.Instance.InventoryEvent.PlayerItemHeldEvent += OnPlayerItemHeldEvent;
            EventInstance.Instance.InventoryEvent.PlayerInventoryUpdateEvent += OnPlayerInventoryUpdateEvent;
        }

        private void OnDisable()
        {
            EventInstance.Instance.InventoryEvent.PlayerItemHeldEvent -= OnPlayerItemHeldEvent;
            EventInstance.Instance.InventoryEvent.PlayerInventoryUpdateEvent -= OnPlayerInventoryUpdateEvent;
        }

        private void OnPlayerItemHeldEvent(Core.entity.Player player, int arg2, int arg3)
        {
            SwapItem(player.GetInventory().GetItemInHand());
        }

        private void OnPlayerInventoryUpdateEvent(IPlayerInventory playerInventory)
        {
            SwapItem(playerInventory.GetItemInHand());
        }

        private void SwapItem(ItemStack itemStack)
        {
            //TODO: POOLING OBJECT
            //CLEAR MODEL
            foreach (Transform model in _playerHand.HandTransform)
                Destroy(model.gameObject);

            if (itemStack == null) return;

            //SET MODEL
            Instantiate(handItemPack.GetWeaponModel(itemStack.GetType()), _playerHand.HandTransform);
        }
    }
}