﻿using System.raycast;
using UnityEngine;

namespace Player.raycast
{
    public class CameraRayProvider : MonoBehaviour, IRayProvider
    {
        private Camera _camera;

        private void Awake()
        {
            _camera = Camera.main;
        }

        public Ray GetRay()
        {
            int x = Screen.width / 2;
            int y = Screen.height / 2;
            return _camera.ScreenPointToRay(new Vector2(x,y));
        }
    }
}