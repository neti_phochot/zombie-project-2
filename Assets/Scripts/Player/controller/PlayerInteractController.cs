﻿using Core.events;
using Core.inventory.InventoryView;
using UnityEngine;

namespace Player.controller
{
    public class PlayerInteractController : MonoBehaviour
    {
        [SerializeField] private PlayerInteract playerInteract;

        private void Start()
        {
            EventInstance.Instance.InventoryEvent.InventoryOpenEvent += OnInventoryOpenEvent;
            EventInstance.Instance.InventoryEvent.InventoryCloseEvent += OnInventoryCloseEvent;
        }


        private void OnDestroy()
        {
            EventInstance.Instance.InventoryEvent.InventoryOpenEvent -= OnInventoryOpenEvent;
            EventInstance.Instance.InventoryEvent.InventoryCloseEvent -= OnInventoryCloseEvent;
        }

        private void OnInventoryOpenEvent(IInventoryView inventoryView)
        {
            playerInteract.enabled = false;
        }

        private void OnInventoryCloseEvent()
        {
            playerInteract.enabled = true;
        }
    }
}