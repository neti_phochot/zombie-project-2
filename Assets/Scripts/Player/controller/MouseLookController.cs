﻿using Core.events;
using Core.inventory.InventoryView;
using Core.spectator;
using UnityEngine;
using ZombieProject.events;

namespace Player.controller
{
    public class MouseLookController : MonoBehaviour
    {
        [SerializeField] private MouseLook mouseLook;

        private ISpectator _spectator;

        private void Awake()
        {
            _spectator = GetComponent<ISpectator>();
        }

        private void OnEnable()
        {
            EventInstance.Instance.InventoryEvent.InventoryOpenEvent += OnInventoryOpenEvent;
            EventInstance.Instance.InventoryEvent.InventoryCloseEvent += OnInventoryCloseEvent;
            GameEventInstance.Instance.GameEvent.GameOverEvent += OnGameOverEvent;
            EventInstance.Instance.UIEvent.ToggleMenuEvent += OnToggleMenuEvent;
            EventInstance.Instance.PlayerEvent.PlayerSpectateEvent += OnPlayerSpectateEvent;
        }

        private void OnDisable()
        {
            EventInstance.Instance.InventoryEvent.InventoryOpenEvent -= OnInventoryOpenEvent;
            EventInstance.Instance.InventoryEvent.InventoryCloseEvent -= OnInventoryCloseEvent;
            GameEventInstance.Instance.GameEvent.GameOverEvent -= OnGameOverEvent;
            EventInstance.Instance.UIEvent.ToggleMenuEvent -= OnToggleMenuEvent;
            EventInstance.Instance.PlayerEvent.PlayerSpectateEvent -= OnPlayerSpectateEvent;
        }

        private bool _isGameOver;
        private void OnInventoryOpenEvent(IInventoryView inventoryView)
        {
            mouseLook.enabled = false;
        }

        private void OnInventoryCloseEvent()
        {
            mouseLook.enabled = true;
        }

        private void OnGameOverEvent(bool win)
        {
            mouseLook.enabled = false;
            _isGameOver = true;
        }

        private void OnToggleMenuEvent(bool show)
        {
            mouseLook.enabled = !show;
        }

        private void OnPlayerSpectateEvent(ISpectator spectator)
        {
            if (_isGameOver) return;
            mouseLook.enabled = _spectator == spectator;
        }
    }
}