﻿using Core.events;
using Core.inventory.InventoryView;
using Core.ui;
using Player.crosshair;
using UnityEngine;
using ZombieProject.events;
using ZombieProject.game.specialitem.drone;

namespace Player.controller
{
    public class CrosshairController : BaseUI
    {
        [SerializeField] private InventoryEventSO inventoryEventSo;
        [SerializeField] private Transform crossHair;

        protected override void OnEnable()
        {
            base.OnEnable();
            inventoryEventSo.InventoryOpenEvent += OnInventoryOpenEvent;
            inventoryEventSo.InventoryCloseEvent += OnInventoryCloseEvent;
            GameEventInstance.Instance.SpecialItemEvent.DroneActiveEvent += OnDroneActiveEvent;
            GameEventInstance.Instance.SpecialItemEvent.DroneDeActiveEvent += OnDroneDeActiveEvent;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            inventoryEventSo.InventoryOpenEvent -= OnInventoryOpenEvent;
            inventoryEventSo.InventoryCloseEvent -= OnInventoryCloseEvent;
            GameEventInstance.Instance.SpecialItemEvent.DroneActiveEvent -= OnDroneActiveEvent;
            GameEventInstance.Instance.SpecialItemEvent.DroneDeActiveEvent -= OnDroneDeActiveEvent;
        }

        private void OnDroneActiveEvent(Drone drone)
        {
            Crosshair.Instance.SetCrosshair(CrosshairType.DRONE);
        }

        private void OnDroneDeActiveEvent(Drone drone)
        {
            Crosshair.Instance.ResetDefault();
        }

        private void OnInventoryOpenEvent(IInventoryView inventoryView)
        {
            crossHair.gameObject.SetActive(false);
        }

        private void OnInventoryCloseEvent()
        {
            crossHair.gameObject.SetActive(true);
        }
    }
}