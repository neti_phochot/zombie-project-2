﻿using Core.events;
using Core.inventory;
using UnityEngine;
using ZombieProject.weapon.inventory.item;

namespace Player.controller
{
    public class PlayerWeaponController : MonoBehaviour
    {
        private Core.entity.Player _player;

        private void Awake()
        {
            _player = FindObjectOfType<Core.entity.Player>();
        }

        private void Start()
        {
            EventInstance.Instance.PlayerEvent.PlayerInteractEvent += PlayerInteractEvent;
            EventInstance.Instance.PlayerEvent.PlayerInteractObjectEvent += PlayerInteractObjectEvent;
        }

        private void Update()
        {
            ReloadWeapon();
        }

        private void OnDestroy()
        {
            EventInstance.Instance.PlayerEvent.PlayerInteractEvent -= PlayerInteractEvent;
            EventInstance.Instance.PlayerEvent.PlayerInteractObjectEvent -= PlayerInteractObjectEvent;
        }

        private void ReloadWeapon()
        {
            if (!Input.GetKeyDown(KeyCode.R)) return;
            if (!(_player.GetInventory().GetItemInHand() is WeaponCommandItem weaponCommandItem)) return;
            weaponCommandItem.GetWeapon().Reload();
        }

        private void PlayerInteractObjectEvent(Core.entity.Player player, RaycastHit raycastHit, ActionType actionType,
            ItemStack itemStack)
        {
            if (actionType == ActionType.RIGHT_CLICK_DOWN || actionType == ActionType.RIGHT_CLICK_UP) return;
            OnInteractItem(player, actionType, itemStack);
        }

        private void PlayerInteractEvent(Core.entity.Player player, ActionType actionType, ItemStack itemStack)
        {
            OnInteractItem(player, actionType, itemStack);
        }

        private void OnInteractItem(Core.entity.Player player, ActionType actionType, ItemStack itemStack)
        {
            if (!(itemStack is WeaponCommandItem weaponCommandItem)) return;
            weaponCommandItem.GetWeapon().Interact(actionType);
        }
    }
}