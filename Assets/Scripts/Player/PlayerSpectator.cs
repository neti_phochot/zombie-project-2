﻿using Core.events;
using Core.spectator;
using Singleton;

namespace Player
{
    public class PlayerSpectator : MonoSingleton<PlayerSpectator>
    {
        private Core.entity.Player _player;

        public override void Awake()
        {
            base.Awake();
            _player = FindObjectOfType<Core.entity.Player>();
        }

        private ISpectator _spectator;

        private void Start()
        {
            SetCamera(_player);
        }

        public void SetCamera(ISpectator spectator)
        {
            _player.CloseInventory();
            if (_spectator != null) Spectator.SetSpectate(_spectator, spectator);
            else Spectator.SetSpectate(spectator);
            _spectator = spectator;
            EventInstance.Instance.PlayerEvent.OnPlayerSpectateEvent(spectator);
        }
    }
}