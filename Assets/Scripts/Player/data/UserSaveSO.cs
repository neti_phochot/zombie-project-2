﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ZombieProject.weapon;

namespace Player.data
{
    [CreateAssetMenu(menuName = "User Save", order = 0)]
    public class UserSaveSO : ScriptableObject
    {
        [Serializable]
        public class UserData
        {
            public string username;
            public int levelUnlocked;
            public int points;
            public List<WeaponSo> purchasedItem;
            public List<WeaponSo> equippedItem;

            public UserData(UserSaveSO userSave)
            {
                username = userSave.username;
                levelUnlocked = userSave.levelUnlocked;
                points = userSave.points;
                //purchasedItem = userSave.purchasedItem;
                //equippedItem = userSave.equippedItem;
            }
        }

        public string username;
        public int levelUnlocked;
        public int points;
        public List<WeaponSo> purchasedItem = new List<WeaponSo>();
        public List<WeaponSo> equippedItem = new List<WeaponSo>();

        public void Clear()
        {
            levelUnlocked = 0;
            points = 0;
            purchasedItem.Clear();
            equippedItem.Clear();
        }
    }
}