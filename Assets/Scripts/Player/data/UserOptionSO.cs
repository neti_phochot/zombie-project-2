﻿using System;
using UnityEngine;

namespace Player.data
{
    [CreateAssetMenu(menuName = "Player Option", order = 0)]
    public class UserOptionSO : ScriptableObject
    {
        public  Action<float> OnSensitivityChanged; 
        public  Action<bool> OnSoundOn; 
        public  Action<bool> OnMusicOn;

        private float _sensitivity = 1;
        private bool _isMusicOn = true;
        private bool _isSoundOn = true;
        public bool IsMusicOn
        {
            set
            {
                _isMusicOn = value;
                OnMusicOn?.Invoke(value);
            }
            get => _isMusicOn;
        } 
        public bool IsSoundOn
        {
            set
            {
                _isSoundOn = value;
                OnSoundOn?.Invoke(value);
            }
            get => _isSoundOn;
        } 
        
        public float Sensitivity
        {
            set
            {
                _sensitivity = value;
                OnSensitivityChanged?.Invoke(value);
            }
            get => _sensitivity;
        } 
    }
}