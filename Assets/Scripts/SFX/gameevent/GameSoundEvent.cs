﻿using System;
using Core.entity;
using Core.events;
using Core.inventory;
using UnityEngine;
using ZombieProject.events;
using ZombieProject.game.wave;
using ZombieProject.tower;
using ZombieProject.weapon;

namespace SFX.gameevent
{
    public class GameSoundEvent : MonoBehaviour
    {
        [Header("GAME")] [SerializeField] private EnumValue waveStart;
        [SerializeField] private EnumValue enemyEnterDefencePoint;
        [SerializeField] private EnumValue coin;
        [Header("ENEMY")] [SerializeField] private EnumValue damage;
        [SerializeField] private EnumValue enemyDead;
        [Header("PLAYER")] [SerializeField] private EnumValue playerRespawn;
        [SerializeField] private EnumValue playerDead;
        [SerializeField] private EnumValue interactObject;
        [Header("WEAPON")] [SerializeField] private EnumValue reloading;
        [SerializeField] private EnumValue reloaded;
        [SerializeField] private EnumValue projectileHit;
        [SerializeField] private EnumValue explodeProjectileHit;
        [Header("TOWER")] [SerializeField] private EnumValue towerBuildStart;
        [SerializeField] private EnumValue towerBuildFinished;
        [Header("UI")] [SerializeField] private EnumValue buttonClick;

        private void OnDisable()
        {
            EventInstance.Instance.EntityEvent.EntityDamageModifiedEvent -= OnEntityDamageModifiedEvent;
            EventInstance.Instance.EntityEvent.EntityDamageEvent -= OnEntityDamageEvent;
            EventInstance.Instance.EntityEvent.EntityDeathEvent -= OnEntityDeathEvent;
            GameEventInstance.Instance.GameEvent.WaveStartEvent -= OnWaveStartEvent;
            GameEventInstance.Instance.GameEvent.EnemyEnterDefencePointEvent -= OnEnemyEnterDefencePointEvent;
            GameEventInstance.Instance.GameEvent.PlayerRespawnEvent -= OnPlayerRespawnEvent;
            GameEventInstance.Instance.GameEvent.CoinPickupEvent -= OnCoinChangeEvent;
            EventInstance.Instance.PlayerEvent.PlayerHitMonsterEvent -= OnPlayerDeath;
            EventInstance.Instance.PlayerEvent.PlayerInteractObjectEvent -= OnPlayerInteractObjectEvent;
            GameEventInstance.Instance.WeaponEvent.ReloadEvent -= OnReloadEvent;
            GameEventInstance.Instance.WeaponEvent.ReloadedEvent -= OnReloadedEvent;
            GameEventInstance.Instance.WeaponEvent.ShootEvent -= OnShootEvent;
            GameEventInstance.Instance.WeaponEvent.ProjectileHitEvent -= OnProjectileHitEvent;
            GameEventInstance.Instance.WeaponEvent.ExplodeProjectileHitEvent -= OnExplodeProjectileHitEvent;
            GameEventInstance.Instance.TowerEvent.TowerBuildStartEvent -= OnTowerBuildStartEvent;
            GameEventInstance.Instance.TowerEvent.TowerBuildCompletedEvent -= OnTowerBuildCompletedEvent;
            EventInstance.Instance.UIEvent.ButtonEnterEvent -= OnButtonEnterEvent;
            EventInstance.Instance.UIEvent.ButtonClickEvent -= OnButtonClickEvent;
        }

        private void OnEnable()
        {
            //ENEMY
            EventInstance.Instance.EntityEvent.EntityDamageModifiedEvent += OnEntityDamageModifiedEvent;
            EventInstance.Instance.EntityEvent.EntityDamageEvent += OnEntityDamageEvent;
            EventInstance.Instance.EntityEvent.EntityDeathEvent += OnEntityDeathEvent;
            GameEventInstance.Instance.GameEvent.EnemyEnterDefencePointEvent += OnEnemyEnterDefencePointEvent;

            //GAME
            GameEventInstance.Instance.GameEvent.WaveStartEvent += OnWaveStartEvent;
            GameEventInstance.Instance.GameEvent.CoinPickupEvent += OnCoinChangeEvent;

            //PLAYER
            EventInstance.Instance.PlayerEvent.PlayerHitMonsterEvent += OnPlayerDeath;
            GameEventInstance.Instance.GameEvent.PlayerRespawnEvent += OnPlayerRespawnEvent;
            EventInstance.Instance.PlayerEvent.PlayerInteractObjectEvent += OnPlayerInteractObjectEvent;

            //WEAPON
            GameEventInstance.Instance.WeaponEvent.ReloadEvent += OnReloadEvent;
            GameEventInstance.Instance.WeaponEvent.ReloadedEvent += OnReloadedEvent;
            GameEventInstance.Instance.WeaponEvent.ShootEvent += OnShootEvent;
            GameEventInstance.Instance.WeaponEvent.ProjectileHitEvent += OnProjectileHitEvent;
            GameEventInstance.Instance.WeaponEvent.ExplodeProjectileHitEvent += OnExplodeProjectileHitEvent;

            //TOWER
            GameEventInstance.Instance.TowerEvent.TowerBuildStartEvent += OnTowerBuildStartEvent;
            GameEventInstance.Instance.TowerEvent.TowerBuildCompletedEvent += OnTowerBuildCompletedEvent;

            //UI
            EventInstance.Instance.UIEvent.ButtonEnterEvent += OnButtonEnterEvent;
            EventInstance.Instance.UIEvent.ButtonClickEvent += OnButtonClickEvent;
        }

        private void OnButtonEnterEvent()
        {
            //SoundInstance.Instance.PlaySound(buttonEnter);
        }

        private void OnButtonClickEvent()
        {
            SoundInstance.Instance.PlaySound(buttonClick);
        }

        private void OnTowerBuildStartEvent(Tower obj)
        {
            SoundInstance.Instance.PlaySound(towerBuildStart, obj.GetLocation());
        }

        private void OnTowerBuildCompletedEvent(Tower obj)
        {
            SoundInstance.Instance.PlaySound(towerBuildFinished, obj.GetLocation());
        }

        private void OnProjectileHitEvent(ContactPoint contactPoint)
        {
            SoundInstance.Instance.PlaySound(projectileHit, contactPoint.point, 0.5f);
        }

        private void OnExplodeProjectileHitEvent(ContactPoint contactPoint)
        {
            SoundInstance.Instance.PlaySound(explodeProjectileHit, contactPoint.point, 0.5f);
        }

        #region WEAPON

        private void OnShootEvent(IWeapon weapon)
        {
            var weaponConfig = weapon.GetWeaponSo();
            SoundInstance.Instance.PlaySound(weaponConfig.soundType, weapon.GetShootTransform().position, 0.7f);
        }

        private void OnReloadedEvent(IWeapon weapon)
        {
            SoundInstance.Instance.PlaySound(reloaded, weapon.GetShootTransform().position);
        }

        private void OnReloadEvent(IWeapon weapon)
        {
            SoundInstance.Instance.PlaySound(reloading, weapon.GetShootTransform().position);
        }

        #endregion

        #region PLAYER

        private void OnPlayerInteractObjectEvent(Core.entity.Player arg1, RaycastHit arg2, ActionType arg3,
            ItemStack arg4)
        {
            SoundInstance.Instance.PlaySound(interactObject);
        }

        private void OnPlayerDeath(Monster obj)
        {
            SoundInstance.Instance.PlaySound(playerDead);
        }

        private void OnPlayerRespawnEvent()
        {
            SoundInstance.Instance.PlaySound(playerRespawn);
        }

        #endregion

        #region GAME

        private void OnWaveStartEvent(IWaveManager obj)
        {
            SoundInstance.Instance.PlaySound(waveStart);
        }

        private void OnEnemyEnterDefencePointEvent(IWaveManager arg1, Monster arg2)
        {
            SoundInstance.Instance.PlaySound(enemyEnterDefencePoint);
        }

        private void OnCoinChangeEvent(int amount)
        {
            SoundInstance.Instance.PlaySound(coin);
        }

        #endregion

        #region ENEMY

        private bool _hitDelay;

        private void OnEntityDamageModifiedEvent(LivingEntity livingEntity, float dmg, bool modified)
        {
            if (_hitDelay) return;
            SetDelay(() => { _hitDelay = true; }, () => { _hitDelay = false; });
            SoundInstance.Instance.PlaySound(damage, livingEntity.GetLocation());
        }

        private void OnEntityDamageEvent(LivingEntity livingEntity, float dmg)
        {
            if (_hitDelay) return;
            SetDelay(() => { _hitDelay = true; }, () => { _hitDelay = false; });
            SoundInstance.Instance.PlaySound(damage, livingEntity.GetLocation());
        }

        private void OnEntityDeathEvent(LivingEntity livingEntity)
        {
            SoundInstance.Instance.PlaySound(enemyDead, livingEntity.GetLocation());
        }

        #endregion

        private void SetDelay(Action startAction, Action endAction)
        {
            startAction?.Invoke();
            LeanTween.value(0, 1, 0.0005f).setOnComplete(() => { endAction?.Invoke(); });
        }
    }
}