﻿using System;
using System.Collections.Generic;
using Player.data;
using Singleton;
using UnityEngine;
using Random = UnityEngine.Random;

namespace SFX
{
    public class SoundInstance : ResourceSingleton<SoundInstance>
    {
        [SerializeField] private SoundData[] soundData;
        [SerializeField] private UserOptionSO userOptionSo;

        [Serializable]
        public struct SoundData
        {
            public EnumValue soundType;
            public AudioClip[] sounds;
        }

        public override void Awake()
        {
            base.Awake();
            InitSound();
        }

        private Dictionary<EnumValue, AudioClip[]> _sounds;

        private void InitSound()
        {
            _sounds = new Dictionary<EnumValue, AudioClip[]>();
            foreach (var data in soundData)
            {
                if (_sounds.ContainsKey(data.soundType)) continue;
                _sounds.Add(data.soundType, data.sounds);
            }
        }

        private bool HasSound(EnumValue soundType)
        {
            return _sounds.ContainsKey(soundType);
        }

        public void PlaySound(EnumValue soundType, Vector3 location, float volume = 1f)
        {
            if (!userOptionSo.IsSoundOn) return;
            if (!HasSound(soundType)) return;
            var sounds = _sounds[soundType];
            var audioSource = LeanAudio.playClipAt(sounds[Random.Range(0, sounds.Length)], location);
            audioSource.volume = volume;
            audioSource.spatialBlend = 0.5f;
        }

        public void PlaySound(EnumValue soundType, float volume = 1f)
        {
            if (!userOptionSo.IsSoundOn) return;
            if (!HasSound(soundType)) return;
            var sounds = _sounds[soundType];
            LeanAudio.play(sounds[Random.Range(0, sounds.Length)]).volume = volume;
        }
    }
}