﻿namespace ZombieProject.ui.healthpoint
{
    public interface IHealthPointDisplay
    {
        void SetHealthPoint(int previous, int current);
    }
}