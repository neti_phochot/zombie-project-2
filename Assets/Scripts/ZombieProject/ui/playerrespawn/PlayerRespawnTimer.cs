﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace ZombieProject.ui.playerrespawn
{
    public class PlayerRespawnTimer : MonoBehaviour, IPlayerRespawnTimer
    {
        [SerializeField] private Image respawnTimerImage;
        [SerializeField] private TextMeshProUGUI respawnTimerText;

        public void SetRespawnTimer(float current, float delay)
        {
            respawnTimerText.text = $"{current:F0}";
            respawnTimerImage.fillAmount = Mathf.Clamp01(current / delay);
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}