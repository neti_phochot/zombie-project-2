﻿using ZombieProject.game.wave;

namespace ZombieProject.ui.nextwaveinfo
{
    public interface INextWaveInfo
    {
        void SetWaveInfo(WaveSo waveSo);
        void ClearInfo();
    }
}