﻿using Core.entity;
using Core.events;
using UnityEngine;

namespace ZombieProject.ui.hitmarker
{
    public class HitMarker : MonoBehaviour
    {
        [SerializeField] private CanvasGroup indicatorCanvasGroup;
        [SerializeField] private float stayLenght = 0.2f;
        [SerializeField] private LeanTweenType fadeType = LeanTweenType.easeOutBounce;

        private void OnEnable()
        {
            EventInstance.Instance.EntityEvent.EntityDamageEvent += OnEntityDamageEvent;
        }

        private void OnDisable()
        {
            EventInstance.Instance.EntityEvent.EntityDamageEvent -= OnEntityDamageEvent;
        }

        private void OnEntityDamageEvent(LivingEntity livingEntity, float damage)
        {
            if (!(livingEntity is Monster)) return;
            PlayHitEffect();
        }

        private LTDescr _descr;

        private void PlayHitEffect()
        {
            if (_descr != null)
            {
                LeanTween.cancel(gameObject);
            }

            _descr = LeanTween.value(1, 0, stayLenght)
                .setEase(fadeType)
                .setOnUpdate(v => { indicatorCanvasGroup.alpha = v; });
        }
    }
}