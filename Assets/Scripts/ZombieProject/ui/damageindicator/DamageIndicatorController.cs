﻿using Core.entity;
using Core.events;
using UnityEngine;

namespace ZombieProject.ui.damageindicator
{
    public class DamageIndicatorController : MonoBehaviour
    {
        [SerializeField] private Color noneModifiedDamageColor = Color.red;
        [SerializeField] private Color modifiedDamageColor = Color.yellow;

        private void OnEnable()
        {
            EventInstance.Instance.EntityEvent.EntityDamageModifiedEvent += OnEntityDamageModifiedEvent;
        }

        private void OnDisable()
        {
            EventInstance.Instance.EntityEvent.EntityDamageModifiedEvent -= OnEntityDamageModifiedEvent;
        }

        private void OnEntityDamageModifiedEvent(LivingEntity livingEntity, float damage, bool isModified)
        {
            if (!(livingEntity is Monster)) return;
            DamageIndicatorInstance.SetDamage(livingEntity.GetEyeTransform().position, damage,
                isModified ? modifiedDamageColor : noneModifiedDamageColor);
        }
    }
}