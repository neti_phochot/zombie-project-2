﻿using Core.ui;
using ZombieProject.events;

namespace ZombieProject.ui.waveTimer
{
    public class WaveTimerUI : BaseUI
    {
        private IWaveTimer _waveTimer;

        public override void Awake()
        {
            base.Awake();
            _waveTimer = GetComponent<IWaveTimer>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            GameEventInstance.Instance.GameEvent.WaveTimerTickEvent += OnWaveTimerTickEvent;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            GameEventInstance.Instance.GameEvent.WaveTimerTickEvent -= OnWaveTimerTickEvent;
        }

        private void OnWaveTimerTickEvent(float current, float waveDelay)
        {
            _waveTimer.SetWaveTimer(current, waveDelay);
        }
    }
}