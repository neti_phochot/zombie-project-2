﻿namespace ZombieProject.ui.weaponbar
{
    public interface IWeaponReloadDisplay
    {
        void SetReloading(float time, float reloadTIme);
    }
}