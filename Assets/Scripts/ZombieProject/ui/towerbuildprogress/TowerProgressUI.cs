﻿using Core.ui;
using UnityEngine;
using ZombieProject.events;
using ZombieProject.tower;
using ZombieProject.ui.progressbar;

namespace ZombieProject.ui.towerbuildprogress
{
    public class TowerProgressUI : BaseUI
    {
        private Tower _tower;
        private IProgressBar _progressBar;

        public override void Awake()
        {
            base.Awake();
            _progressBar = GetComponent<IProgressBar>();
        }

        public void Init(Tower tower)
        {
            _tower = tower;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            GameEventInstance.Instance.TowerEvent.TowerBuildInProgressEvent += OnTowerBuildInProgressEvent;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            GameEventInstance.Instance.TowerEvent.TowerBuildInProgressEvent -= OnTowerBuildInProgressEvent;
        }

        private void OnTowerBuildInProgressEvent(Tower tower, float current, float buildTime)
        {
            if (_tower != tower) return;
            _progressBar.SetProgress(current, buildTime);
        }
    }
}