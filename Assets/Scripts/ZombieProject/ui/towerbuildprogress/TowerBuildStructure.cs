﻿using UnityEngine;
using ZombieProject.events;
using ZombieProject.tower;

namespace ZombieProject.ui.towerbuildprogress
{
    public class TowerBuildStructure : MonoBehaviour
    {
        [SerializeField] private GameObject[] buildingStatePrefabs;

        private void OnEnable()
        {
            GameEventInstance.Instance.TowerEvent.TowerBuildStartEvent += OnTowerBuildStartEvent;
        }

        private void OnDisable()
        {
            GameEventInstance.Instance.TowerEvent.TowerBuildStartEvent -= OnTowerBuildStartEvent;
        }

        private void OnTowerBuildStartEvent(Tower tower)
        {
            float stateCount = buildingStatePrefabs.Length;
            var stateTime = 2f / stateCount;
            var stateIndex = 0;

            BuildState();

            void BuildState()
            {
                GameObject structure = null;
                LeanTween.value(0, 1, stateTime)
                    .setOnStart(() =>
                    {
                        var stateTransform = tower.transform;
                        structure = Instantiate(buildingStatePrefabs[stateIndex++], stateTransform.position,
                            stateTransform.rotation);
                        structure.SetActive(true);
                    })
                    .setOnComplete(() =>
                    {
                        Destroy(structure);
                        if (stateIndex < stateCount)
                        {
                            BuildState();
                        }
                    });
            }
        }
    }
}