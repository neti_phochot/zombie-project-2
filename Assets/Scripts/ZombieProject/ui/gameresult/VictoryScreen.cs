﻿using System;
using Core.ui.gamebutton;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;
using Utils.scene;

namespace ZombieProject.ui.gameresult
{
    public class VictoryScreen : MonoBehaviour, IVictoryScreen
    {
        [SerializeField] private TextMeshProUGUI rewardText;
        [SerializeField] private TextMeshProUGUI timeText;
        [SerializeField] private GameButton continueButton;
        [SerializeField] private GameButton restartButton;

        [Header("Stars")] [SerializeField] private Image starImage;
        [SerializeField] private float oneStarFill = 0.3f;
        [SerializeField] private float twoStarFill = 0.69f;
        [SerializeField] private float threeStarFill = 1f;

        private void Awake()
        {
            continueButton.SetClickAction(() => { GameSceneManager.LoadScene(GameSceneType.MAP); });
            restartButton.SetClickAction(GameSceneManager.RestartCurrentScene);
        }

        public void SetReward(int points)
        {
            rewardText.text = $"+{points} GOLD";
        }

        public void SetScore(float scoreFactor)
        {
            float fillAmount;
            if (scoreFactor < 0.6f)
            {
                fillAmount = oneStarFill;
            }
            else if (scoreFactor < 1f)
            {
                fillAmount = twoStarFill;
            }
            else
            {
                fillAmount = threeStarFill;
            }

            starImage.fillAmount = fillAmount;
        }

        public void SetRecord(int value)
        {
            timeText.text = $"Coin Collected: {value:N0}";
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}