﻿using Core.ui.gamebutton;
using UnityEngine;
using Utils.scene;

namespace ZombieProject.ui.gameresult
{
    public class DefeatScreen : MonoBehaviour, IDefeatScreen
    {
        [SerializeField] private GameButton restartButton;
        [SerializeField] private GameButton quitButton;

        private void Awake()
        {
            quitButton.SetClickAction(() => { GameSceneManager.LoadScene(GameSceneType.MAP); });
            restartButton.SetClickAction(GameSceneManager.RestartCurrentScene);
        }

        public void Play()
        {
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}