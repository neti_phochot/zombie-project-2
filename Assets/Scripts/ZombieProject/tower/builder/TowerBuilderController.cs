﻿using System.Linq;
using Core.entity;
using Core.events;
using Core.inventory.InventoryView;
using UnityEngine;
using ZombieProject.game;
using ZombieProject.tower.inventory.item;

namespace ZombieProject.tower.builder
{
    [RequireComponent(typeof(Tower))]
    public class TowerBuilderController : MonoBehaviour, IInteractable
    {
        [SerializeField] private InventoryEventSO inventoryEventSo;
        private Tower _tower;

        private ITowerBuilder _towerBuilder;
        private ITowerBuilderUpgradeResponse _towerBuilderUpgradeResponse;

        private WaveManager _waveManager;

        private void Awake()
        {
            _towerBuilder = GetComponent<ITowerBuilder>();
            _towerBuilderUpgradeResponse = GetComponent<ITowerBuilderUpgradeResponse>();
            _tower = GetComponent<Tower>();
            _waveManager = FindObjectOfType<WaveManager>();
        }

        private void Start()
        {
            inventoryEventSo.InventoryClickEvent += OnInventoryClickEvent;
        }

        private void OnDestroy()
        {
            inventoryEventSo.InventoryClickEvent -= OnInventoryClickEvent;
        }

        public void Interact(ActionType actionType, Entity source)
        {
            if (actionType != ActionType.RIGHT_CLICK_DOWN) return;
            if (!(source is Core.entity.Player player)) return;
            player.OpenInventory(_tower.GetInventory());
        }

        private void OnInventoryClickEvent(IInventoryView inventoryView)
        {
            inventoryView.GetPlayer().CloseInventory();
            if (!(inventoryView.GetCursor() is TowerCommandItem towerCommandItem)) return;
            if (!_tower.GetInventory().GetContents().Contains(towerCommandItem)) return;
            _towerBuilderUpgradeResponse.OnUpgrade(_waveManager, _towerBuilder, towerCommandItem);
        }
    }
}