﻿using Core.inventory;
using UnityEngine;
using ZombieProject.events;
using ZombieProject.tower.factory;
using ZombieProject.ui.towerbuildprogress;

namespace ZombieProject.tower.builder
{
    public class TowerBuilder : MonoBehaviour, ITowerBuilder
    {
        private ITowerCommandItemFactory _towerCommandItemFactory;

        private void Awake()
        {
            _towerCommandItemFactory = GetComponent<ITowerCommandItemFactory>();
        }

        public void Build(TowerSo newTowerSo)
        {
            //DESTROY OLD TOWER
            Destroy(gameObject);

            //CREATE TOWER
            var thisTowerTransform = transform;
            var newTower = Instantiate(newTowerSo.towerPrefab, thisTowerTransform.position,
                thisTowerTransform.rotation);

            //ADD TOWER COMMAND ITEM
            var newTowerItem = new ItemStack[newTowerSo.upgradeTowers.Length];
            for (var i = 0; i < newTowerSo.upgradeTowers.Length; i++)
            {
                var upgradeTowerSo = newTowerSo.upgradeTowers[i];
                newTowerItem[i] = _towerCommandItemFactory.Create(upgradeTowerSo);
            }

            newTower.GetInventory().SetStorageContents(newTowerItem);

            //BUILDING STATE
            if (newTowerSo.instantBuild) return;
            const float buildTime = 2f;
            newTower.gameObject.SetActive(false);
            var progressUI = TowerProgressInstance.SetTowerProgress(newTower, TowerProgressBarType.DEFAULT);
            progressUI.transform.position = newTower.transform.position + new Vector3(0, 2.5f, 0);
            GameEventInstance.Instance.TowerEvent.OnTowerBuildStartEvent(newTower);
            LeanTween.value(0, buildTime, buildTime)
                .setOnUpdate(v =>
                {
                    GameEventInstance.Instance.TowerEvent.OnTowerBuildInProgressEvent(newTower, v, buildTime);
                }).setOnComplete(() =>
                {
                    newTower.gameObject.SetActive(true);
                    Destroy(progressUI.gameObject);

                    GameEventInstance.Instance.TowerEvent.OnTowerBuildCompletedEvent(newTower);
                });
        }
    }
}