﻿using UnityEngine;
using ZombieProject.game;
using ZombieProject.tower.inventory.item;

namespace ZombieProject.tower.builder
{
    public class TowerBuilderUpgradeResponse : MonoBehaviour, ITowerBuilderUpgradeResponse
    {
        public void OnUpgrade(WaveManager waveManager, ITowerBuilder towerBuilder,
            TowerCommandItem towerCommandItem)
        {
            var towerSo = towerCommandItem.GetTowerSo();
            if (towerSo.forSell)
            {
                Debug.Log($"YOU SELL THIS TOWER FOR: {towerSo.price}");
                waveManager.Coin += towerSo.price;
            }
            else
            {
                if (waveManager.Coin < towerSo.price)
                {
                    Debug.Log("YOU CANNOT AFFORD THIS ITEM!");
                    return;
                }

                waveManager.Coin -= towerSo.price;
            }

            towerBuilder.Build(towerCommandItem.GetTowerSo());
        }
    }
}