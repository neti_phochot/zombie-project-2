﻿using Core.entity;
using Core.entity.livingentity;
using UnityEngine;
using ZombieProject.ui.healthbar;

namespace ZombieProject.tower.weapon
{
    public class TowerArtillery : MonoBehaviour
    {
        [SerializeField] private BarrackSo barrackSo;
        [SerializeField] private Transform[] spawnPoints;
        [SerializeField] private int spawnInterval = 3;

        private Soldier[] _soldiers;
        private float _timer;
        private Tower _tower;

        private void Awake()
        {
            _soldiers = new Soldier[spawnPoints.Length];
            _tower = GetComponent<Tower>();
        }

        private void Update()
        {
            _timer -= Time.deltaTime;
            if (_timer > 0) return;
            _timer = spawnInterval;
            SpawnSoldier();
        }

        private void SpawnSoldier()
        {
            for (var i = 0; i < spawnPoints.Length; i++)
            {
                var soldier = _soldiers[i];
                if (soldier && !soldier.IsDead()) continue;

                var spawnedSolider =
                    (Soldier) EntityInstance.Instance.SpawnEntity(EntityType.SOLDIER, spawnPoints[i].position);
                _soldiers[i] = spawnedSolider;
                spawnedSolider.transform.rotation = _tower.transform.rotation;
                spawnedSolider.transform.SetParent(_tower.transform);

                //SET HEALTH
                spawnedSolider.SetHealth(barrackSo.maxHealth);
                spawnedSolider.SetMaxHealth(barrackSo.maxHealth);

                //SET HEALTH BAR
                var bar = HealthBarInstance.SetHealthBar(spawnedSolider, HealthBarType.DEFAULT,
                    spawnedSolider.GetEyeTransform().localPosition + new Vector3(0, 0.5f));
                bar.transform.SetParent(spawnedSolider.transform);

                //SET WEAPON
                spawnedSolider.GetComponent<SoldierMeleeAttackResponse>().Init(barrackSo);
            }
        }
    }
}