﻿using System.Collections.Generic;
using Core.entity;

namespace ZombieProject.tower.weapon
{
    public interface ITowerWeaponTargetProvider
    {
        public void FindTarget(IEnumerable<Mob> targets, float range, ITowerWeaponTargetResponse weaponTargetResponse);
    }
}