﻿using System.Diagnostics;
using Core.entity;
using Core.ui.mainmenu.leaderboard;
using Player.data;
using UnityEngine;
using Utils.scene;
using ZombieProject.events;
using ZombieProject.game.wave;
using Debug = UnityEngine.Debug;

namespace ZombieProject.game
{
    public class WaveManager : MonoBehaviour, IWaveManager
    {
        [SerializeField] private UserSaveSO userSaveSo;
        [SerializeField] private int victoryReward;

        [Header("PLAYER SETTINGS")] [SerializeField]
        private int startBalance;

        [SerializeField] private int startHealthPoints;

        [Header("WAVES SETTINGS")] [SerializeField]
        private WaveSo[] waves;

        [SerializeField] private float waveTimerUpdateThreshold;

        private int _coin;
        private Wave _currentWave;
        private int _healthPoint;
        private int _maxWave;
        private int _wave;
        private int _waveIndex;
        private IWavePath _wavePath;

        private Wave[] _waves;
        private IWaveSpawner _waveSpawner;
        private WaveState _waveState;
        private GameState _gameState;

        private Stopwatch _stopwatch;

        public void Awake()
        {
            _stopwatch = new Stopwatch();
            _waveSpawner = GetComponent<IWaveSpawner>();
            _wavePath = GetComponent<IWavePath>();

            Debug.Assert(_waveSpawner != null, "IWaveSpawner!=null");
            Debug.Assert(_wavePath != null, "IWavePath!=null");

            _waveSpawner.Init(this);

            _waveState = WaveState.WAITING;
            _gameState = GameState.WAITING;

            InitWaves();
        }


        private void Start()
        {
            Coin = startBalance;
            HealthPoint = startHealthPoints;
            MaxHealthPPoint = startHealthPoints;
        }

        private void Update()
        {
            UpdateWave();
        }

        public int TotalCoinPickup { get; set; }
        public int Time => (int) _stopwatch.ElapsedMilliseconds;

        public int Coin
        {
            get => _coin;
            set
            {
                GameEventInstance.Instance.GameEvent.OnCoinChangedEvent(this, _coin, value);
                _coin = value;
            }
        }

        public int HealthPoint
        {
            get => _healthPoint;
            set
            {
                GameEventInstance.Instance.GameEvent.OnHealthPointChangedEvent(this, _healthPoint, value);
                _healthPoint = value;
            }
        }

        public int MaxHealthPPoint { get; private set; }

        public void NextWave()
        {
            if (_gameState == GameState.ENDED) return;

            _wave++;
            _waveState = WaveState.RUNNING;
            _gameState = GameState.RUNNING;

            _currentWave = _waves[_waveIndex++];
            _currentWave.StartWave();

            var isFirstWave = _waveIndex == 1;
            if (isFirstWave)
            {
                _stopwatch.Start();
                Debug.Log("FIRST WAVE STARTED!");
            }

            GameEventInstance.Instance.GameEvent.OnWaveStartEvent(this);
        }

        public WaveSo GetNextWaveSo()
        {
            var next = _waveIndex + 1;
            if (next >= _waves.Length) return null;
            var nextWave = _waves[next];
            return nextWave.GetWaveSo();
        }

        public int GetWave()
        {
            return _wave;
        }

        public int GetMaxWave()
        {
            return _maxWave;
        }

        public bool IsFinalWave()
        {
            return _wave == _maxWave;
        }

        public void GameOver()
        {
            if (_gameState == GameState.ENDED) return;
            _gameState = GameState.ENDED;
            GameEventInstance.Instance.GameEvent.OnGameOverEvent(false);
        }

        public void Win()
        {
            if (_gameState == GameState.ENDED) return;
            _gameState = GameState.ENDED;

            //ADD REWARDS
            userSaveSo.points += victoryReward;

            //UNLOCK LEVEL
            var nextLevel = (int) GameSceneManager.CurrentLevel + 1;
            if (nextLevel > userSaveSo.levelUnlocked)
            {
                userSaveSo.levelUnlocked = nextLevel;
            }

            GameEventInstance.Instance.GameEvent.OnGameOverEvent(true);

            //ADD LEADERBOARD
            _stopwatch.Stop();
            LeaderboardInstance.Instance.AddScore(GameSceneManager.CurrentLevel, TotalCoinPickup, c => { });
        }

        public int GetReward()
        {
            return victoryReward;
        }

        private void UpdateWave()
        {
            if (_waveState != WaveState.RUNNING) return;
            _currentWave.UpdateWave();
        }

        private void InitWaves()
        {
            _waves = new Wave[waves.Length];
            for (var i = 0; i < _waves.Length; i++)
            {
                var waveSo = waves[i];
                var wave = new Wave(waveSo, this, _waveSpawner, _wavePath, waveTimerUpdateThreshold);
                _waves[i] = wave;
            }

            _maxWave = _waves.Length;
        }

        public void CheckEnemyEnterDefencePoint(Monster monster)
        {
            if (!_waveSpawner.IsWaveEnemy(monster)) return;
            GameEventInstance.Instance.GameEvent.OnEnemyEnterDefencePointEvent(this, monster);
        }

        private void OnDestroy()
        {
            LeanTween.cancelAll();
        }
    }
}