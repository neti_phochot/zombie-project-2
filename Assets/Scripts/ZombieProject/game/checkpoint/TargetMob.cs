﻿using Core.entity;

namespace ZombieProject.game.checkpoint
{
    public class TargetMob : LivingEntity, ICheckPoint
    {
        private TargetMob _nextTargetMob;

        protected override void Awake()
        {
            base.Awake();
            _isDead = true;
        }

        public void SetNextTarget(TargetMob targetMob)
        {
            _nextTargetMob = targetMob;
        }

        public TargetMob GetNextTarget()
        {
            return _nextTargetMob;
        }
    }
}