﻿using Core.ui;
using UnityEngine;
using ZombieProject.events;
using ZombieProject.ui.weaponbar;
using ZombieProject.weapon;

namespace ZombieProject.game.specialitem.drone.ui
{
    public class DroneUI : BaseUI
    {
        [SerializeField] private WeaponEventSo weaponEventSo;

        private IWeaponAmmoDisplay _weaponAmmoDisplay;
        private IWeaponReloadDisplay _weaponReloadDisplay;

        private IWeapon _weapon;

        public override void Awake()
        {
            base.Awake();
            _weaponAmmoDisplay = GetComponent<IWeaponAmmoDisplay>();
            _weaponReloadDisplay = GetComponent<IWeaponReloadDisplay>();
        }

        protected override void Start()
        {
            base.Start();
            Hide();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            weaponEventSo.AmmoChangeEvent += OnAmmoChangeEvent;
            weaponEventSo.ReloadingEvent += OnReloadingEvent;
            GameEventInstance.Instance.SpecialItemEvent.DroneActiveEvent += OnDroneActiveEvent;
            GameEventInstance.Instance.SpecialItemEvent.DroneDeActiveEvent += OnDroneDeActiveEvent;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            weaponEventSo.AmmoChangeEvent -= OnAmmoChangeEvent;
            weaponEventSo.ReloadingEvent -= OnReloadingEvent;
            GameEventInstance.Instance.SpecialItemEvent.DroneActiveEvent -= OnDroneActiveEvent;
            GameEventInstance.Instance.SpecialItemEvent.DroneDeActiveEvent -= OnDroneDeActiveEvent;
        }

        private void OnAmmoChangeEvent(IWeapon weapon, int amount)
        {
            if (_weapon != weapon) return;
            _weaponAmmoDisplay.SetAmmo(weapon);
        }

        private void OnReloadingEvent(IWeapon weapon, float reload, float reloadTime)
        {
            if (_weapon != weapon) return;
            _weaponReloadDisplay.SetReloading(reload, reloadTime);
        }

        private void OnDroneActiveEvent(Drone dn)
        {
            _weapon = dn.GetWeapon();
            OnAmmoChangeEvent(_weapon, _weapon.GetAmmo());
            Show();
        }

        private void OnDroneDeActiveEvent(Drone dn)
        {
            Hide();
        }
    }
}