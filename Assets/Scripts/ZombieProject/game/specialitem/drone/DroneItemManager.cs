﻿using Core.events;
using Player;
using UnityEngine;
using ZombieProject.events;

namespace ZombieProject.game.specialitem.drone
{
    public class DroneItemManager : MonoBehaviour
    {
        [Header("DRONE SETTINGS")] [SerializeField]
        private Drone drone;

        [SerializeField] private float droneDelay;
        [SerializeField] private float droneControlTime;

        private Core.entity.Player _player;
        private float _droneTimer;
        private float _droneControlTimer;
        private bool _isDroneReady;

        private void Awake()
        {
            _player = FindObjectOfType<Core.entity.Player>();
        }

        private void OnEnable()
        {
            GameEventInstance.Instance.GameEvent.GameOverEvent += OnGameOverEvent;
            GameEventInstance.Instance.SpecialItemEvent.DroneActiveEvent += OnDroneActiveEvent;
            GameEventInstance.Instance.SpecialItemEvent.DroneDeActiveEvent += OnDroneDeActiveEvent;
            EventInstance.Instance.InputEvent.SpecialEvent += OnSpecialEvent;
        }

        private void OnDisable()
        {
            GameEventInstance.Instance.GameEvent.GameOverEvent -= OnGameOverEvent;
            GameEventInstance.Instance.SpecialItemEvent.DroneActiveEvent -= OnDroneActiveEvent;
            GameEventInstance.Instance.SpecialItemEvent.DroneDeActiveEvent -= OnDroneDeActiveEvent;
            EventInstance.Instance.InputEvent.SpecialEvent -= OnSpecialEvent;
        }

        private void Start()
        {
            _droneTimer = droneDelay;
            _droneControlTimer = droneControlTime;
        }

        private void OnGameOverEvent(bool win)
        {
            PlayerSpectator.Instance.SetCamera(_player);
        }

        private void OnDroneActiveEvent(Drone dn)
        {
            _droneControlTimer = droneControlTime;
        }

        private void OnDroneDeActiveEvent(Drone dn)
        {
            _droneTimer = droneDelay;
            _isDroneReady = false;
        }


        private void OnSpecialEvent()
        {
            if (drone.IsActive)
            {
                PlayerSpectator.Instance.SetCamera(_player);
            }
            else if (_isDroneReady)
            {
                PlayerSpectator.Instance.SetCamera(drone);
            }
        }

        private void Update()
        {
            if (drone.IsActive) CheckDroneControl();
            else CheckDroneItem();
        }

        private void CheckDroneItem()
        {
            if (_isDroneReady) return;
            _droneTimer -= Time.deltaTime;
            GameEventInstance.Instance.SpecialItemEvent.OnDroneTimerEvent(_droneTimer, droneDelay);
            if (_droneTimer > 0) return;
            _droneTimer = droneDelay;
            _isDroneReady = true;
        }

        private void CheckDroneControl()
        {
            _droneControlTimer -= Time.deltaTime;
            GameEventInstance.Instance.SpecialItemEvent.OnDroneTimerEvent(_droneControlTimer, droneControlTime);
            if (_droneControlTimer > 0) return;
            _droneControlTimer = droneControlTime;
            PlayerSpectator.Instance.SetCamera(_player);
        }
    }
}