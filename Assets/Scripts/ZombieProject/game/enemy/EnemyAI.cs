﻿using Core.entity;
using UnityEngine;
using ZombieProject.game.checkpoint;

namespace ZombieProject.game.enemy
{
    public class EnemyAI : MonoBehaviour, IEnemyAI
    {
        private TargetMob _targetMob;
        private LivingEntity _enemy;
        private Monster _monster;

        private void Awake()
        {
            _monster = GetComponent<Monster>();
        }

        protected void FixedUpdate()
        {
            UpdateCheckPoint();
        }

        public void SetCheckPoint(TargetMob targetMob)
        {
            _targetMob = targetMob;
            _monster.SetTarget(targetMob);
        }

        public void Attack(LivingEntity enemy)
        {
            _enemy = enemy;
        }

        private void UpdateCheckPoint()
        {
            //TODO: FIX NESTED STATEMENT

            //ATTACK ENEMY
            if (_enemy)
            {
                if (_enemy.IsDead())
                {
                    _enemy = null;
                    return;
                }

                if (_monster.GetTarget() == _enemy) return;
                _monster.SetTarget(_enemy);
                return;
            }

            //GO TO NEXT CHECK POINT ON REACTS
            if (!_targetMob) return;
            _monster.SetTarget(_targetMob);

            if (Vector3.Distance(_targetMob.GetLocation(), _monster.GetLocation()) > 3f) return;
            var nextPoint = _targetMob.GetNextTarget();
            if (!nextPoint)
            {
                Destroy(this);
                return;
            }

            SetCheckPoint(nextPoint);
        }
    }
}