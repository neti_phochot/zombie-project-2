﻿using Core.entity;
using UnityEngine;

namespace ZombieProject.game.defencepoint
{
    public class DefencePoint : MonoBehaviour
    {
        private WaveManager _waveManager;

        private void Awake()
        {
            _waveManager = FindObjectOfType<WaveManager>();
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.TryGetComponent<Monster>(out var monster)) return;
            other.gameObject.SetActive(false);
            _waveManager.CheckEnemyEnterDefencePoint(monster);
        }
    }
}