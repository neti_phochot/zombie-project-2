﻿namespace ZombieProject.game.wave
{
    public interface IWaveManager
    {
        public int Coin { get; set; }
        public int HealthPoint { get; set; }
        int GetWave();
        int GetMaxWave();
        bool IsFinalWave();
        void NextWave();
        WaveSo GetNextWaveSo(); 
        void GameOver();
        void Win();
        int GetReward();
    }
}