﻿using ZombieProject.game.checkpoint;

namespace ZombieProject.game.wave
{
    public interface IWavePath
    {
        TargetMob GetStartPoint(EntranceType entranceType, PathType pathType);
    }
}