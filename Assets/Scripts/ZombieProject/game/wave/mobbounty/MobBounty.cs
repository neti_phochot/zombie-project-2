﻿using System.Collections.Generic;
using Coin;
using Core.entity;
using UnityEngine;
using ZombieProject.events;

namespace ZombieProject.game.wave.mobbounty
{
    public class MobBounty : MonoBehaviour
    {
        [SerializeField] private MobBountySO mobBountyTable;
        [SerializeField] private float coinPadding = 0.2f;
        [SerializeField] private int worthPerCoin = 5;

        private Dictionary<EntityType, int> _mobBountyTable;
        private WaveManager _waveManager;

        public void Awake()
        {
            _waveManager = FindObjectOfType<WaveManager>();
            InitMobBounty();
        }

        private void OnEnable()
        {
            GameEventInstance.Instance.GameEvent.EnemyKilledEvent += OnEnemyKilledEvent;
        }

        private void OnDisable()
        {
            GameEventInstance.Instance.GameEvent.EnemyKilledEvent -= OnEnemyKilledEvent;
        }

        private void InitMobBounty()
        {
            _mobBountyTable = new Dictionary<EntityType, int>();
            foreach (var data in mobBountyTable.MobBountyRewardTable)
            {
                if (_mobBountyTable.ContainsKey(data.entityType)) continue;
                _mobBountyTable.Add(data.entityType, data.reward);
            }
        }

        private void OnEnemyKilledEvent(Monster monster)
        {
            var reward = _mobBountyTable.ContainsKey(monster.GetEntityType())
                ? _mobBountyTable[monster.GetEntityType()]
                : 0;
            if (reward < 1) return;

            _waveManager.Coin += worthPerCoin;
            var spawnCoin = reward / worthPerCoin;
            WorldItemInstance.Instance.SpawnWorldCoins(
                () =>
                {
                    var bonus = 1;
                    //var bonus = Mathf.RoundToInt(worthPerCoin * 0.5f);
                    _waveManager.Coin += bonus;
                    _waveManager.TotalCoinPickup++;
                    GameEventInstance.Instance.GameEvent.OnCoinPickupEvent(bonus);
                }, spawnCoin, coinPadding, monster.GetLocation());
        }
    }
}