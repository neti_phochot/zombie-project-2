﻿using System;
using Core.entity;
using UnityEngine;
using ZombieProject.events;
using ZombieProject.game.wave;

namespace ZombieProject.game
{
    public class WaveController : MonoBehaviour
    {
        [SerializeField] private EnemyDamageTable[] enemyDamageTables;

        [Serializable]
        public struct EnemyDamageTable
        {
            public EntityType entityType;
            public int damage;
        }

        private void OnEnable()
        {
            GameEventInstance.Instance.GameEvent.WaveClearedEvent += OnWaveClearedEvent;
            GameEventInstance.Instance.GameEvent.WaveEndedEvent += OnWaveEndedEvent;
            GameEventInstance.Instance.GameEvent.EnemyEnterDefencePointEvent += OnEnemyEnterDefencePointEvent;
            GameEventInstance.Instance.GameEvent.HealthPointChangeEvent += OnHealthPointChangeEvent;
        }

        private void OnDisable()
        {
            GameEventInstance.Instance.GameEvent.WaveClearedEvent -= OnWaveClearedEvent;
            GameEventInstance.Instance.GameEvent.WaveEndedEvent -= OnWaveEndedEvent;
            GameEventInstance.Instance.GameEvent.EnemyEnterDefencePointEvent -= OnEnemyEnterDefencePointEvent;
            GameEventInstance.Instance.GameEvent.HealthPointChangeEvent -= OnHealthPointChangeEvent;
        }

        private void OnWaveClearedEvent(IWaveManager waveManager)
        {
            if (!waveManager.IsFinalWave()) return;
            waveManager.Win();
        }

        private void OnWaveEndedEvent(IWaveManager waveManager)
        {
            if (waveManager.IsFinalWave()) return;
            waveManager.NextWave();
        }

        private void OnEnemyEnterDefencePointEvent(IWaveManager waveManager, Monster monster)
        {
            foreach (var damageTable in enemyDamageTables)
            {
                if (damageTable.entityType != monster.GetEntityType()) continue;
                waveManager.HealthPoint -= damageTable.damage;
            }
        }

        private void OnHealthPointChangeEvent(IWaveManager waveManager, int previous, int current)
        {
            if (current > 0) return;
            waveManager.GameOver();
        }
    }
}