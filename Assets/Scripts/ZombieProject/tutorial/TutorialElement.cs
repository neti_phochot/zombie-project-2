﻿using UnityEngine;

namespace ZombieProject.tutorial
{
    public abstract class TutorialElement : MonoBehaviour, IVisible
    {
        public abstract void Show();
        public abstract void Hide();
    }
}