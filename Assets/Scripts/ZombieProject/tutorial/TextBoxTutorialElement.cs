﻿using UnityEngine;

namespace ZombieProject.tutorial
{
    public class TextBoxTutorialElement : TutorialElement
    {
        [SerializeField] private LeanTweenType animeType;
        [SerializeField] private Vector3 scaleTo;
        [SerializeField] private float scaleTime;

        public override void Show()
        {
            gameObject.SetActive(true);
            LeanTween.scale(gameObject, scaleTo, scaleTime)
                .setEase(animeType)
                .setLoopPingPong();
        }

        public override void Hide()
        {
            gameObject.SetActive(false);
            LeanTween.cancel(gameObject);
        }
    }
}