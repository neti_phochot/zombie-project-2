﻿using Cinemachine;
using Core.spectator;
using Player;

namespace ZombieProject.tutorial
{
    public class CamTutorialElement : TutorialElement, ISpectator
    {
        private CinemachineVirtualCamera _cinemachineVirtualCamera;

        private void Awake()
        {
            _cinemachineVirtualCamera = GetComponent<CinemachineVirtualCamera>();
        }

        public override void Show()
        {
            gameObject.SetActive(true);
            PlayerSpectator.Instance.SetCamera(this);
        }

        public override void Hide()
        {
            gameObject.SetActive(false);
        }


        public CinemachineVirtualCamera GetCamera()
        {
            return _cinemachineVirtualCamera;
        }
    }
}