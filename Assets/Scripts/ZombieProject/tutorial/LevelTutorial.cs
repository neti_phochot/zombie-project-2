using System;
using System.Collections.Generic;
using Core.ui.gamebutton;
using TMPro;
using UnityEngine;
using Utils;
using Utils.scene;

namespace ZombieProject.tutorial
{
    public class LevelTutorial : MonoBehaviour
    {
        private static bool _isComplete;

        [SerializeField] private TutorialData[] tutorialData;
        [SerializeField] private GameButton nextButton;
        [SerializeField] private GameButton skipButton;
        [SerializeField] private TextMeshProUGUI buttonText;

        [Serializable]
        public class TutorialData
        {
            public TutorialElement[] tutorialElements;
        }

        private Queue<TutorialData> _tutorialQueues;
        private TutorialData _currentData;

        private void Awake()
        {
            if (_isComplete)
            {
                Destroy(gameObject);
                return;
            }

            InitTutorial();
            nextButton.SetClickAction(NextTutorial);
            skipButton.SetClickAction(TutorialCompleted);
            Invoke(nameof(NextTutorial), 0.2f);
        }

        private void InitTutorial()
        {
            _tutorialQueues = new Queue<TutorialData>(tutorialData);
        }

        private void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                GameCursor.Show();
            }
        }

        private void NextTutorial()
        {
            if (_tutorialQueues.Count < 1)
            {
                TutorialCompleted();
                return;
            }

            GameCursor.Show();
            nextButton.gameObject.SetActive(true);

            if (_currentData != null)
            {
                HidePreviousElement();
            }

            _currentData = _tutorialQueues.Dequeue();
            var compete = tutorialData.Length - _tutorialQueues.Count;
            var isComplete = compete == tutorialData.Length;
            buttonText.text = isComplete ? "Complete!" : $"Next {compete}/{tutorialData.Length}!";

            foreach (var data in _currentData.tutorialElements)
                data.Show();
        }

        private void HidePreviousElement()
        {
            foreach (var data in _currentData.tutorialElements)
                data.Hide();
        }

        private void TutorialCompleted()
        {
            HidePreviousElement();
            _isComplete = true;

            GameSceneManager.RestartCurrentScene();
        }
    }
}