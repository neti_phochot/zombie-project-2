﻿using Core.projectile;
using Core.projectile.rayprojectile;
using UnityEngine;

namespace ZombieProject.weapon
{
    public class WeaponProjectileLauncher : MonoBehaviour, IWeaponProjectileLauncher
    {
        public ProjectileBase LaunchProjectile(IWeapon weapon)
        {
            var shootTransform = weapon.GetShootTransform();
            var weaponSo = weapon.GetWeaponSo();
            var shooter = weapon.GetShooter();
            var velocity = shootTransform.forward * weaponSo.speed;
            var projectile = ProjectileInstance.Create(weapon.GetWeaponSo().projectileType);
            var randDamage = Random.Range(weaponSo.minDamage, weaponSo.maxDamage);
            projectile.Init(shooter, shootTransform, randDamage, velocity, weapon.GetWeaponSo().projectileType);
            return projectile;
        }
    }
}