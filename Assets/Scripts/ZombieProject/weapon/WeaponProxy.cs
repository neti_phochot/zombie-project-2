﻿using System;
using UnityEngine;
using ZombieProject.events;

namespace ZombieProject.weapon
{
    public class WeaponProxy : MonoBehaviour, IWeaponProxy
    {
        private float _time;

        private void Update()
        {
            if (_time > 0)
            {
                _time -= Time.deltaTime;
            }
        }

        public void TriggerProxy(IWeapon weapon, IWeaponProjectileLauncher weaponProjectileLauncher)
        {
            if (weapon.IsReloading) return;
            if (weapon.GetAmmo() < 1) return;

            if (_time > 0)
            {
                _time -= Time.deltaTime;
                return;
            }

            _time = weapon.GetWeaponSo().fireRate;

            weapon.SetAmmo(weapon.GetAmmo() - 1);
            weaponProjectileLauncher.LaunchProjectile(weapon);
            GameEventInstance.Instance.WeaponEvent.OnShootEvent(weapon);
        }
    }
}