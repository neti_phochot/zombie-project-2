﻿using Core.entity;
using Core.events;
using UnityEngine;
using ZombieProject.events;

namespace ZombieProject.weapon
{
    public class Weapon : MonoBehaviour, IWeapon
    {
        [SerializeField] private WeaponEventSo weaponEventSo;
        private int _ammo;

        private bool _isReloading;

        private int _maxAmmo;
        private float _reloadTimeLeft;
        private Entity _shooter;
        private Transform _shootTransform;
        private IWeaponInteract _weaponInteract;

        private IWeaponProjectileLauncher _weaponProjectileLauncher;
        private IWeaponProxy _weaponProxy;
        private IWeaponReload _weaponReload;
        private WeaponSo _weaponSo;

        private void Awake()
        {
            _weaponProjectileLauncher = GetComponent<IWeaponProjectileLauncher>();
            _weaponInteract = GetComponent<IWeaponInteract>();
            _weaponProxy = GetComponent<IWeaponProxy>();
            _weaponReload = GetComponent<IWeaponReload>();
        }

        public bool IsReloading
        {
            get => _isReloading;
            set
            {
                if (value) weaponEventSo.OnReloadEvent(this);
                else weaponEventSo.OnReloadedEvent(this);
                _isReloading = value;
            }
        }

        public float ReloadTimeLeft
        {
            get => _reloadTimeLeft;
            set
            {
                weaponEventSo.OnReloadingEvent(this, value, _weaponSo.reloadTime);
                _reloadTimeLeft = value;
            }
        }

        public bool HasAmmo()
        {
            return _ammo > 0;
        }

        public bool IsFull()
        {
            return _ammo == _maxAmmo;
        }

        public Entity GetShooter()
        {
            return _shooter;
        }

        public Transform GetShootTransform()
        {
            return _shootTransform;
        }

        public WeaponSo GetWeaponSo()
        {
            return _weaponSo;
        }

        public void Shoot()
        {
            _weaponProxy.TriggerProxy(this, _weaponProjectileLauncher);
        }

        public void Reload()
        {
            _weaponReload.Reload(this);
        }

        public int GetAmmo()
        {
            return _ammo;
        }

        public void SetAmmo(int ammo)
        {
            _ammo = ammo;
            weaponEventSo.OnAmmoChangeEvent(this, _ammo);
        }

        public int GetMaxAmmo()
        {
            return _maxAmmo;
        }

        public void SetMaxAmmo(int maxAmmo)
        {
            _maxAmmo = maxAmmo;
            weaponEventSo.OnAmmoChangeEvent(this, _ammo);
        }

        public void Init(WeaponSo weaponSo, Entity shooter, Transform launchTransform)
        {
            _weaponSo = weaponSo;
            _shooter = shooter;
            _ammo = weaponSo.maxAmmo;
            _maxAmmo = weaponSo.maxAmmo;

            _shootTransform = launchTransform;
            transform.SetParent(shooter.transform);
        }

        public void Interact(ActionType actionType)
        {
            _weaponInteract.OnInteract(this, actionType);
        }
    }
}