﻿using System;
using Core.entity;
using UnityEngine;
using ZombieProject.game.wave;

namespace ZombieProject.events
{
    [CreateAssetMenu(menuName = "TD/Game Event", order = 0)]
    public class GameEventSo : ScriptableObject
    {
        //PLAYER
        public event Action<IWaveManager, int, int> CoinChangeEvent;
        public event Action<int> CoinPickupEvent;
        public event Action<IWaveManager, int, int> HealthPointChangeEvent;
        public event Action PlayerRespawnEvent;
        public event Action<float, float> PlayerRespawnTimerEvent;

        public void OnCoinChangedEvent(IWaveManager waveManager, int previous, int current)
        {
            CoinChangeEvent?.Invoke(waveManager, previous, current);
        }

        public void OnCoinPickupEvent(int amount)
        {
            CoinPickupEvent?.Invoke(amount);
        }

        public void OnHealthPointChangedEvent(IWaveManager waveManager, int previous, int current)
        {
            HealthPointChangeEvent?.Invoke(waveManager, previous, current);
        }

        public void OnPlayerRespawnEvent()
        {
            PlayerRespawnEvent?.Invoke();
        }

        public void OnPlayerRespawnTimerEvent(float timer, float delay)
        {
            PlayerRespawnTimerEvent?.Invoke(timer, delay);
        }

        //ENEMY
        public event Action<Monster> EnemyKilledEvent;
        public event Action<IWaveManager, Monster> EnemyEnterDefencePointEvent;

        public void OnEnemyKilledEvent(Monster monster)
        {
            EnemyKilledEvent?.Invoke(monster);
        }

        public void OnEnemyEnterDefencePointEvent(IWaveManager waveManager, Monster monster)
        {
            EnemyEnterDefencePointEvent?.Invoke(waveManager, monster);
        }

        //WAVE
        public event Action<IWaveManager> WaveStartEvent;
        public event Action<float, float> WaveTimerTickEvent;
        public event Action WaveEventCompletedEvent;
        public event Action<IWaveManager> WaveClearedEvent;
        public event Action<IWaveManager> WaveEndedEvent;

        public void OnWaveStartEvent(IWaveManager waveManager)
        {
            WaveStartEvent?.Invoke(waveManager);
        }

        public void OnWaveTimerTickEvent(float timer, float waveDelay)
        {
            WaveTimerTickEvent?.Invoke(timer, waveDelay);
        }

        /// <summary>
        /// Trigger when all enemy wave event spawned
        /// </summary>
        public void OnWaveSpawnCompletedEvent()
        {
            WaveEventCompletedEvent?.Invoke();
        }

        /// <summary>
        /// Trigger when all enemies killed
        /// </summary>
        /// <param name="waveManager"></param>
        public void OnWaveClearedEvent(IWaveManager waveManager)
        {
            WaveClearedEvent?.Invoke(waveManager);
        }

        /// <summary>
        /// Trigger when wave delay reacted
        /// </summary>
        /// <param name="waveManager"></param>
        public void OnWaveEndedEvent(IWaveManager waveManager)
        {
            WaveEndedEvent?.Invoke(waveManager);
        }

        //GAME
        /// <summary>
        /// Trigger when all wave cleared
        /// </summary>
        public event Action<bool> GameOverEvent;

        public void OnGameOverEvent(bool win)
        {
            GameOverEvent?.Invoke(win);
        }
    }
}