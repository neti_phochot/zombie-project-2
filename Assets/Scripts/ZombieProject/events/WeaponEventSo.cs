﻿using System;
using UnityEngine;
using ZombieProject.weapon;

namespace ZombieProject.events
{
    [CreateAssetMenu(menuName = "TD/Weapon Event", order = 0)]
    public class WeaponEventSo : ScriptableObject
    {
        public event Action<IWeapon> ReloadEvent;
        public event Action<IWeapon, float, float> ReloadingEvent;
        public event Action<IWeapon> ReloadedEvent;
        public event Action<IWeapon, int> AmmoChangeEvent;
        public event Action<IWeapon> ShootEvent;
        public event Action<ContactPoint> ProjectileHitEvent;
        public event Action<ContactPoint> ExplodeProjectileHitEvent;

        public void OnProjectileHitEvent(ContactPoint contactPoint)
        {
            ProjectileHitEvent?.Invoke(contactPoint);
        }

        public void OnExplodeProjectileHitEvent(ContactPoint contactPoint)
        {
            ExplodeProjectileHitEvent?.Invoke(contactPoint);
        }

        public void OnAmmoChangeEvent(IWeapon weapon, int ammo)
        {
            AmmoChangeEvent?.Invoke(weapon, ammo);
        }

        public void OnReloadedEvent(IWeapon weapon)
        {
            ReloadedEvent?.Invoke(weapon);
        }

        public void OnReloadingEvent(IWeapon weapon, float timeLeft, float reloadTime)
        {
            ReloadingEvent?.Invoke(weapon, timeLeft, reloadTime);
        }

        public void OnReloadEvent(IWeapon weapon)
        {
            ReloadEvent?.Invoke(weapon);
        }

        public void OnShootEvent(IWeapon weapon)
        {
            ShootEvent?.Invoke(weapon);
        }
    }
}