﻿using System;
using UnityEngine;
using ZombieProject.tower;

namespace ZombieProject.events
{
    [CreateAssetMenu(menuName = "TD/Tower Event")]
    public class TowerEventSo : ScriptableObject
    {
        public event Action<Tower, float, float> TowerBuildInProgressEvent;
        public event Action<Tower> TowerBuildStartEvent;
        public event Action<Tower> TowerBuildCompletedEvent;

        public void OnTowerBuildInProgressEvent(Tower tower, float current, float length)
        {
            TowerBuildInProgressEvent?.Invoke(tower, current, length);
        }

        public void OnTowerBuildStartEvent(Tower tower)
        {
            TowerBuildStartEvent?.Invoke(tower);
        }

        public void OnTowerBuildCompletedEvent(Tower tower)
        {
            TowerBuildCompletedEvent?.Invoke(tower);
        }
    }
}